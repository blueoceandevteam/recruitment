<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Roles_model extends CI_Model {
	
	private $_tbl_name = 'roles';
	
	public function role_detail( $id = NULL )
	{
		if( empty( $id ) ){
			$id = $this->input->post('id');
		}
		
		if( !empty( $id ) ){
			$query = $this->db->get_where( $this->_tbl_name , array('id' => $id));
			if( $query->num_rows() > 0 ){
				return $query->row();
			}
		}
		
		return FALSE;
	}
	
	public function delete_role()
	{
		$action = $this->input->post('action');
		$id = $this->input->post('id');
		
		if( $action == 'delete' && !empty( $id ) ) {
			$this->db->delete( $this->_tbl_name , array('id' => $id)); 
			$this->session->set_userdata('delete', $id );
		}
	}
	
	public function save_new()
	{
		$action = $this->input->post('action');
		$code = $this->input->post('code');
		$description = $this->input->post('description');
		
		if( $action == 'save_new' && !empty( $code ) && !empty( $description ) ) {
			
			$id = $this->input->post('id');
			
			$data = array(
				'description' => $description,
				'code'	=> $code,
				'added_by'	=> $this->session->userdata('user_id')
			);
			
			if( empty( $id ) ){
				$data['date_added'] = date('Y-m-d H:i:s');
				$this->db->insert( $this->_tbl_name , $data );
				$this->session->set_userdata('new_role', $this->db->insert_id() );	
			} else {
				$this->db->where('id', $id);
				$this->db->update( $this->_tbl_name , $data );
				$this->session->set_userdata('edit_role', $id );
			}
			
		}
	}
	
	public function check_role_code( $code = NULL )
	{
		if( empty( $code ) ){
			$code = $this->input->post('code');
		}
		
		$id = $this->input->post('id');
		if( empty($id) ) $query = $this->db->get_where($this->_tbl_name, array( 'code' => $code ));
		else $query = $this->db->get_where($this->_tbl_name, array( 'code' => $code, 'id !=' => $id ));
		
		if ( $query->num_rows() > 0 ) return TRUE;
		return FALSE;
	}
	
	public function get_all()
	{
		$query =  $this->db->select( 'id, code, description' )->from( $this->_tbl_name )->order_by('code', 'asc')->get();
		if( $query->num_rows() > 0 ){
			return $query->result();
		} else {
			return FALSE;
		}
	}

	public function get_role_code( $id = 0 )
	{
		$result = '';
		$query =  $this->db->select( 'code' )->from( $this->_tbl_name )->where('id', $id)->get();
		
		if( $query->num_rows() > 0 ){
			$row = $query->row();
			$result = $row->code;
		}
		
		return $result;
	}
	
	public function get_current_user_roles()
	{
		$id = $this->session->userdata('user_id');
		if( empty( $id ) ) $id = 0;
		
		$sql = "SELECT `roles`.`code` FROM `roles` ".
				"INNER JOIN `admin_group_roles` ON `admin_group_roles`.`role_id` = `roles`.`id` ".
				"INNER JOIN `admins_groups` ON `admins_groups`.`group_id` = `admin_group_roles`.`admin_group_id` ".
				"INNER JOIN `admins` on `admins`.`id` = `admins_groups`.`admin_id` ".
				"WHERE `admins`.`id` = ".$id;
		
		$query = $this->db->query( $sql );
		
		if( $query->num_rows() > 0 ){
			
			$result = array();
			foreach ($query->result() as $row) {
				$result[] = $row->code;
			}
			
			return $result;
		}
		
		return array();
	}
}