<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories_model extends CI_Model {
	
	private $_tbl_name = 'categories';
	
	public function category_detail( $id = NULL )
	{
		if( empty( $id ) ){
			$id = $this->input->post('id');
		}
		
		if( !empty( $id ) ){
			$query = $this->db->get_where( $this->_tbl_name , array('id' => $id));
			if( $query->num_rows() > 0 ){
				return $query->row();
			}
		}
		
		return FALSE;
	}
	
	public function delete_category()
	{
		$action = $this->input->post('action');
		$id = $this->input->post('id');
		
		if( $action == 'delete' && !empty( $id ) ) {
			$this->db->delete( $this->_tbl_name , array('id' => $id)); 
			$this->session->set_userdata('delete', $id );
		}
	}
	
	public function save_new()
	{
		$action = $this->input->post('action');
		$code = $this->input->post('code');
		$title = $this->input->post('title');
		
		if( $action == 'save_new' && !empty( $code ) && !empty( $title ) ) {
			
			$id = $this->input->post('id');
			
			$data = array(
				'title' => $title,
				'code'	=> $code,
				'added_by'	=> $this->session->userdata('user_id')
			);
			
			if( empty( $id ) ){
				$data['date_added'] = date('Y-m-d H:i:s');
				$this->db->insert( $this->_tbl_name , $data );
				$this->session->set_userdata('new_category', $this->db->insert_id() );	
			} else {
				$this->db->where('id', $id);
				$this->db->update( $this->_tbl_name , $data );
				$this->session->set_userdata('edit_category', $id );
			}
			
		}
	}
	
	public function check_category_title( $title = NULL )
	{
		if( empty( $title ) ){
			$title = $this->input->post('title');
		}
		
		$id = $this->input->post('id');
		if( empty($id) ) $query = $this->db->get_where($this->_tbl_name, array( 'title' => $title ));
		else $query = $this->db->get_where($this->_tbl_name, array( 'title' => $title, 'id !=' => $id ));
		
		if ( $query->num_rows() > 0 ) return TRUE;
		return FALSE;
	}
	
	public function check_category_code( $code = NULL )
	{
		if( empty( $code ) ){
			$code = $this->input->post('code');
		}
		
		$id = $this->input->post('id');
		if( empty($id) ) $query = $this->db->get_where($this->_tbl_name, array( 'code' => $code ));
		else $query = $this->db->get_where($this->_tbl_name, array( 'code' => $code, 'id !=' => $id ));
		
		if ( $query->num_rows() > 0 ) return TRUE;
		return FALSE;
	}
	
	public function get_all()
	{
		$query =  $this->db->select( 'id, title, code' )->from( $this->_tbl_name )->order_by('title', 'asc')->get();
		if( $query->num_rows() > 0 ){
			return $query->result();
		} else {
			return FALSE;
		}
	}

	public function get_category_code( $cat_id = 0 )
	{
		
		$result = '';
		
		$query =  $this->db->select( 'code' )->from( $this->_tbl_name )->where('id', $cat_id)->get();
		
		if( $query->num_rows() > 0 ){
			$row = $query->row();
			$result = $row->code;
		}
		
		return $result;
	}
	
	public function generate_random_string($length = 10) {
    	
    	$characters = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    	$charactersLength = strlen($characters);
    	$randomString = '';
    	
    	for ($i = 0; $i < $length; $i++) {
    		$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		
		return $randomString;
		
	}
}