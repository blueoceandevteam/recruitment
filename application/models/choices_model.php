<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Choices_model extends CI_Model {
	
	private $_tbl_name = 'exams_question_choices';
	
	public function get_choices_by_exam( $exam_id = 0 ){
		
		if( !empty( $exam_id ) ) {
			
			$query =  $this->db->select('exams_question_choices.*')->from('exams')
						->join('exams_questions', 'exams_questions.exam_id = exams.id', 'inner')
						->join('exams_question_choices', 'exams_question_choices.question_id = exams_questions.id', 'inner')
						->where( 'exams.id', $exam_id )
						->order_by('exams_questions.id','asc')
						->order_by('exams_questions.position','asc')
						->get();
			
			if( $query->num_rows() > 0 ){
				return $query->result();
			}
		}
		
		return FALSE;
	}
	
	public function save_choice()
	{
		$id = $this->input->post('id');
		$question_id = $this->input->post('question_id');
		$name = $this->input->post('name');
		$position = $this->input->post('position');
		$is_correct = $this->input->post('is_correct');
		$type = $this->input->post('type');
		
		if( !empty( $id ) ){
			$data = array(
				'id' => $id
			);
		} elseif( !empty( $question_id ) && !empty( $position )){
			$data = array(
				'question_id'	=> $question_id,
				'position'	=> $position
			);
		}
		
		if( !empty( $data ) ){
			
			$query = $this->db->get_where( $this->_tbl_name , $data);
			
			if( $query->num_rows() > 0 ){
				
				$row = $query->row();
				$id = $row->id;
				if( !empty( $position ) ) $data['position'] = $position;
				$data['name'] = $name;
				$this->db->where('id', $id);
				$this->db->update( $this->_tbl_name , $data);
				 
			} else {
				
				// insert it
				if( !empty( $position ) ) $data['position'] = $position;
				$data['name'] = $name;
				$this->db->set('date_added', 'NOW()', FALSE);
				$this->db->insert( $this->_tbl_name , $data);
				$id = $this->db->insert_id();
			}	
		}
		
		return $id;
	}

	public function delete_choice()
	{
		$id = $this->input->post('id');
		
		if( !empty( $id ) ) {
			
			return $this->db->delete( $this->_tbl_name, array( 'id' => $id ) );
			
		} else {
			
			$question_id = $this->input->post('question_id');
			$position = $this->input->post('position');
			
			if( !empty( $question_id ) && !empty( $position ) ){
				return $this->db->delete( $this->_tbl_name, array( 'question_id' => $question_id, 'position' => $position ) );
			}
			
		}
		
		return FALSE;
	}
	
	public function reposition_choice()
	{
		$id = $this->input->post('id');
		$new_position = $this->input->post('new_position');
		
		$data = NULL;
		$params = NULL;
		if( !empty( $new_position ) ) {
			$data = array( 'position' => $new_position );
		}
		
		if( !empty( $question_id ) ){
			$params = array( 'id' => $id );
		} else {
			$question_id = $this->input->post('question_id');
			$old_position = $this->input->post('old_position');
			if( !empty( $question_id ) && !empty( $old_position ) ){
				$params = array( 'question_id' => $question_id, 'position' => $old_position );
			}
		}
		
		if( !empty( $data ) && !empty( $params ) ){
			return $this->db->update( $this->_tbl_name , $data, $params);
		}
		
		return FALSE;
	}
}