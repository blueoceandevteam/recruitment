<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Applicants_exam_model extends CI_Model {
	
	private $_tbl_names = array( 'applicant' => 'applicants',
								 'jobtitle' => 'categories',
								 'jobtitle_attr' => 'categories_attr',
								 'exams' => 'exams',
								 'questions'=>'exams_questions',
								 'choices'=>'exams_question_choices',
								 'answers'=>'exams_user_answers',
								 'result'=>'exams_user_result' );
	
	public function get_exam_info()
	{
		$exam_info = array();
		$ex_ids = $this->_get_exam_ids();
		foreach($ex_ids as $id):
			$exam_info[] = $this->_get_exam_info($id);
		endforeach;
		return $exam_info;
	}

	/* fetch data */
	public function get_exam_questions($id = false)
	{
		return $this->_get_exam_questions($id);
	}
	
	public function get_exam_question_choices($id) 
	{
		return $this->_get_exam_question_choices($id);
	}

	/* insert data */
	public function insert_user_answer($question_id, $answer) 
	{
		return $this->_insert_user_answer($question_id, $answer);
	}

	#private codes here
	private function _get_exam_info($id)
	{
		$query =  $this->db->select( '*' )
					   ->from( $this->_tbl_names['exams'] )
					   ->where( array('id'=> $id ) )
					   ->get();
		return $query;
	}

	private function _get_exam_ids()
	{
		$exam_reference = $this->_user_exam_reference(); 
		$exam_id = array();
		foreach($exam_reference->result_array() as $exam):
			$exam_id[] = intval($exam['attr_value']);
		endforeach;

		return $exam_id;
	}

	private function _get_exam_questions($id) 
	{
		$user_id = $this->session->userdata('logged_in_applicant');
		if(empty($id))
				return false;

		$query = $this->_get_user_answers($user_id);
		
		$q_count = !empty($query) ? count($query->result_array()) : 0;
		if($q_count > 0):
		foreach($query->result_array() as $done):
			static $taken = '(';
			static $i = 1;
			$taken .= $done['question_id'];
			if($i < $q_count )	
				$taken .= ',';
			else
				$taken .= ')';
			$i++;
		endforeach;
		endif; 

		if($q_count > 0):
			$query =  $this->db->select( '*' )
					   ->from( $this->_tbl_names['questions'] )
					   ->where( "id NOT IN {$taken} AND exam_id = {$id}")
					   ->get();

			return $query;
		else:
			$query  = $this->db->select('*')
					->from( $this->_tbl_names['questions'] )
					->where( array('exam_id' => $id) )
					->get();
			if($query->num_rows() > 0)
				return $query;
		endif;

		return false;
	}

	private function _get_exam_question_choices($id)
	{
		if(empty($id))
				return false;
		$query =  $this->db->select( '*' )
					   ->from( $this->_tbl_names['choices'] )
					   ->where( array('question_id'=> $id ) )
					   ->get();
		if($query->num_rows() > 0)
			return $query;
		else
			return false;
	}

	private function _get_exam_answer($id) 
	{
		if(empty($id))
				return false;

		$query =  $this->db->select( '*' )
					   ->from( $this->_tbl_names['questions'] )
					   ->where( array('question_id'=> $id, 'is_correct'=>1 ) )
					   ->get();
		if($query->num_rows() > 0)
			return $query;
		else
			return false;
	}

	private function _get_user_answers()
	{
		$user_id = $this->session->userdata('logged_in_applicant');
		if(empty($user_id))
				return false;

		$query =  $this->db->select( '*' )
					   ->from( $this->_tbl_names['answers'] )
					   ->where( array( 'user_id' =>  $user_id) )
					   ->get();

		if($query->num_rows() > 0)
			return $query;
		else
			return false;

	}
	private function _insert_user_answer($id, $answer)
	{
		$data = array();
		$user_id = $this->session->userdata('logged_in_applicant');

		$data['user_id']	 =  $user_id;
		$data['question_id'] =  $id;
		$data['name'] 		 =  $answer;

		$insert = $this->db->insert( $this->_tbl_names['answers'], $data ); 
	
		if($insert) 
			return $this->db->insert_id();

		return false;
	}

	private function _user_exam_reference()
	{
		$user_id = $this->session->userdata('logged_in_applicant');
		$query =  $this->db->select( '*' )
					   ->from( $this->_tbl_names['applicant'] . ' a' )
					   ->join( $this->_tbl_names['jobtitle_attr'] . ' b','a.cat_id=b.cat_id','inner')
					   ->where( array('a.id'=> $user_id ) )
					   ->get();
		if($query->num_rows() > 0)
			return $query;
		else
			return false;
	}
}