<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admingrouproles_model extends CI_Model {
	
	private $_tbl_name = 'admin_group_roles';
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_roles_by_group_id( $group_id = NULL )
	{
		if( empty( $group_id ) ){
			$group_id = $this->input->post('group_id');
		}
		
		$query = $this->db->get_where( $this->_tbl_name , array('admin_group_id' => $group_id) );
		
		if( $query->num_rows() > 0 ){
			
			$rows = $query->result();
			$result = array();
			
			foreach ($rows as $row) {
				$result[] = $row->role_id;
			}
			
			return $result;
		}
		
		return FALSE;
	}
	
	public function get_roles_by_user_id( $user_id = NULL )
	{
		
	}
}