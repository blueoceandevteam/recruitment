<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {
	
	private $tbl_name  = 'admins';
	
	public function get_user_id( $username = NULL, $password = NULL ){
		if( !empty($username) && !empty( $password ) ){
			if( !isset( $this->encrypt ) ) $this->load->library('encrypt');
		
			$query = $this->db->get_where($this->tbl_name, array( 'username' => $username ));
			if ( $query->num_rows() > 0 ) {
				$rows = $query->result();
				foreach( $rows as $row ){
					$human_password = $this->encrypt->decode( $row->password );
					if( $password == $human_password ){
						return $row->id;
					}
				}
			} 
		}
		
		return FALSE;
	}
	
	public function check_account( $username, $password ){
		if( !isset( $this->encrypt ) ) $this->load->library('encrypt');
		
		$query = $this->db->get_where($this->tbl_name, array( 'username' => $username ));
		if ( $query->num_rows() > 0 ) {
			$rows = $query->result();
			foreach( $rows as $row ){
				$human_password = $this->encrypt->decode( $row->password );
				if( $password == $human_password ){
					return TRUE;
				}
			}
		} 
		
		return FALSE;
	}
	
	public function create_password( $human_password = NULL ){
		
		if( empty( $human_password ) )
		{
			$human_password = self::_random_string(); 
		}
		
		if( !isset( $this->encrypt ) ) $this->load->library('encrypt');
		return $this->encrypt->encode( $human_password );
	}
	
	private function _random_string( $length = 10 )
	{
		
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		
		return $randomString;
	}
}