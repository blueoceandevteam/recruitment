<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admingroups_model extends CI_Model {
	
	private $_tbl_name = 'admingroups';
	
	public function admingroup_detail( $id = NULL )
	{
		if( empty( $id ) ){
			$id = $this->input->post('id');
		}
		
		if( !empty( $id ) ){
			$query = $this->db->get_where( $this->_tbl_name , array('id' => $id));
			if( $query->num_rows() > 0 ){
				return $query->row();
			}
		}
		
		return FALSE;
	}
	
	public function delete_admingroup()
	{
		$action = $this->input->post('action');
		$id = $this->input->post('id');
		
		if( $action == 'delete' && !empty( $id ) ) {
			// $this->db->delete( $this->_tbl_name , array('id' => $id));
			$data = array( 'deleted' => 1, 'modified_by' => $this->session->userdata('user_id') );
			$this->db->where('id', $id);
			$this->db->update( $this->_tbl_name , $data);  
			$this->session->set_userdata('delete', $id );
		}
	}
	
	public function save_new()
	{
		$action = $this->input->post('action');
		$name = $this->input->post('name');
		$description = $this->input->post('description');
		$name = trim($name);
		$description = trim($description);
		$role_ids = $this->input->post('role_id');
		
		
		if( $action == 'save_new' && !empty( $name ) ) {
			$id = $this->input->post('id');
			
			$data = array(
				'name' => $name,
				'added_by'	=> $this->session->userdata('user_id')
			);
			
			if( empty( $id ) ){
				// search if the data exists before adding new record so that there will be no duplicates
				$query = $this->db->get_where( $this->_tbl_name , array('name' => $name) );
				
				if( $query->num_rows() == 0 ){
					$data['date_added'] = date('Y-m-d H:i:s');
					$data['description'] = $description;
					$this->db->insert( $this->_tbl_name , $data );
					$this->session->set_userdata('new_admingroup', $this->db->insert_id() );
					$id = $this->db->insert_id();
				} else {
					$row = $query->row();
					$id = $row->id;
					if( !empty( $row->deleted ) ){
						$data = array( 'deleted' => 0, 'modified_by' => $this->session->userdata('user_id') );
						$data['description'] = $description;
						$this->db->where('id', $row->id);
						$this->db->update( $this->_tbl_name , $data);
					}
				}
					
			} else {
				$this->db->where('id', $id);
				$data['description'] = $description;
				$this->db->update( $this->_tbl_name , $data );
				$this->session->set_userdata('edit_admingroup', $id );
			}
			
			$this->db->where('admin_group_id', $id);
			$this->db->delete('admin_group_roles');
			
			if( !empty( $role_ids ) ){
				foreach ($role_ids as $role_id) {
					$data = array( 'admin_group_id' => $id, 'role_id' => $role_id );
					$this->db->insert('admin_group_roles', $data);
				}
			}
			
		}
	}
	
	public function check_admingroup_name( $name = NULL )
	{
		if( empty( $name ) ){
			$name = $this->input->post('name');
		}
		
		$id = $this->input->post('id');
		if( empty($id) ) $query = $this->db->get_where($this->_tbl_name, array( 'name' => $name ));
		else $query = $this->db->get_where($this->_tbl_name, array( 'name' => $name, 'id !=' => $id ));
		
		if ( $query->num_rows() > 0 ) return TRUE;
		return FALSE;
	}
	
	public function get_all()
	{
		$params = array( 'deleted' => 0 );
		$query =  $this->db->select( 'id, name' )->from( $this->_tbl_name )->where( $params )->order_by('name', 'asc')->get();
		if( $query->num_rows() > 0 ){
			return $query->result();
		} else {
			return FALSE;
		}
	}

	public function get_admingroup_name( $group_id = 0 )
	{
		
		$result = '';
		
		$query =  $this->db->select( 'name' )->from( $this->_tbl_name )->where('id', $group_id)->get();
		
		if( $query->num_rows() > 0 ){
			$row = $query->row();
			$result = $row->name;
		}
		
		return $result;
	}
}