<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admins_model extends CI_Model {
	
	private $_tbl_name = 'admins';
	
	public function admin_detail( $id = NULL )
	{
		if( empty( $id ) ){
			$id = $this->input->post('id');
		}
		
		if( !empty( $id ) ){
			// $query = $this->db->get_where( $this->_tbl_name , array('id' => $id));
			$sql = " SELECT admins.*, admins_groups.group_id FROM admins LEFT JOIN admins_groups ON admins_groups.admin_id = admins.id WHERE admins.id = ".$id;
			$query = $this->db->query( $sql );
			if( $query->num_rows() > 0 ){
				return $query->row();
			}
		}
		
		return FALSE;
	}
	
	public function delete_admin()
	{
		$action = $this->input->post('action');
		$id = $this->input->post('id');
		
		if( $action == 'delete' && !empty( $id ) ) {
			$data['deleted'] = 1;
			$this->db->where('id', $id);
			$this->db->update( $this->_tbl_name , $data );
			$this->session->set_userdata('delete', $id );
		}
	}
	
	public function save_my_profile()
	{
		$action = $this->input->post('action');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$cpassword = $this->input->post('confirm_password');
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		
		$username = trim($username);
		$password = trim($password);
		$cpassword = trim($cpassword);
		$first_name = trim($first_name);
		$last_name = trim($last_name);
		
		
		if( $action == 'save_my_profile' && !empty( $username ) && !empty( $first_name ) && !empty( $last_name ) ) {
			
			if( !isset( $this->encrypt ) ) $this->load->library('encrypt');
			
			$id = $this->session->userdata('user_id') ;
			
			$data = array(
				'username'		=> $username,
				'first_name'	=> $first_name,
				'last_name'		=> $last_name
			);
			
			if( !empty( $password ) && !empty( $cpassword )) {
				if( $password == $cpassword ) $data['password'] = $this->encrypt->encode( $password );
			}
			
			$data['modified_by'] = $id;
			$this->db->where('id', $id);
			$this->db->update( $this->_tbl_name , $data );
			$this->session->set_userdata('edit_profile', $id );
			
		}
	}
	
	public function save_new()
	{
		$action = $this->input->post('action');
		$group_id = $this->input->post('group_id');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		
		$username = trim($username);
		$password = trim($password);
		$first_name = trim($first_name);
		$last_name = trim($last_name);
		
		
		if( $action == 'save_new' && !empty( $username ) && !empty( $first_name ) && !empty( $last_name ) ) {
			
			if( !isset( $this->encrypt ) ) $this->load->library('encrypt');
			if( !empty( $password ) )  $password = $this->encrypt->encode( $password );
			
			$id = $this->input->post('id');
			
			$data = array(
				'username'		=> $username,
				'first_name'	=> $first_name,
				'last_name'		=> $last_name
			);
			
			if( !empty( $password ) ) {
				$data['password'] = $password;
			}
			
			if( empty( $id ) ){
					
				$data['added_by'] = $this->session->userdata('user_id');
				$data['date_added'] = date('Y-m-d H:i:s');
				$this->db->insert( $this->_tbl_name , $data );
				$this->session->set_userdata('new_admin', $this->db->insert_id() );
				
				// get the current user_id
				$id = $this->db->insert_id();
				
			} else {
				
				$data['modified_by'] = $this->session->userdata('user_id');
				$this->db->where('id', $id);
				$this->db->update( $this->_tbl_name , $data );
				$this->session->set_userdata('edit_admin', $id );
				
			}
			
			// insert or edit the admins_groups
			$query = $this->db->get_where('admins_groups', array('admin_id' => $id) );
			if( $query->num_rows() == 0 ){
				$data = array(
							'admin_id' 		=> $id,
							'group_id' 		=> $group_id,
							'added_by' 		=> $this->session->userdata('user_id'),
							'date_added_by' => date('Y-m-d H:i:s')
						);
				$this->db->insert( 'admins_groups' , $data );
			} else {
				// modify it
				$data = array(
							'group_id'		=> $group_id,
							'modified_by'	=> $this->session->userdata('user_id')
						);
				$this->db->where('admin_id', $id);
				$this->db->update( 'admins_groups' , $data );
			}
		}
	}
	
	public function check_admin_username( $username = NULL )
	{
		if( empty( $username ) ){
			$username = $this->input->post('username');
		}
		
		$id = $this->input->post('id');
		if( empty($id) ) $query = $this->db->get_where($this->_tbl_name, array( 'username' => $username ));
		else $query = $this->db->get_where($this->_tbl_name, array( 'username' => $username, 'id !=' => $id ));
		
		if ( $query->num_rows() > 0 ) return TRUE;
		return FALSE;
	}
	
	public function get_all()
	{
		$query =  $this->db->select( 'id, username, first_name, last_name' )->from( $this->_tbl_name )->where('deleted', 0)->order_by('date_added', 'desc')->get();
		if( $query->num_rows() > 0 ){
			return $query->result();
		} else {
			return FALSE;
		}
	}
	
}