<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Questions_model extends CI_Model {
	
	private $_tbl_name = 'exams_questions';
	
	public function get_questions( $id = 0 )
	{
		$result = FALSE;
		
		if( !empty( $id ) ) {
			$query = $this->db->select()->from( $this->_tbl_name )->where( array('exam_id' => $id) )->order_by('position', 'asc')->get();
			if( $query->num_rows() > 0 ){
				$result = $query->result();
			}
		}
		
		return $result;
	}
	
	public function save_question()
	{
		$id = 0;
		$exam_id = $this->input->post('exam_id');
		$question_id = $this->input->post('question_id');
		$name = $this->input->post('name');
		$position = $this->input->post('position');
		
		if( !empty( $question_id ) ){
			$data = array(
				'id' => $question_id
			);
		} elseif( !empty( $exam_id ) && !empty( $position )){
			$data = array(
				'exam_id'	=> $exam_id,
				'position'	=> $position
			);
		}
		
		if( !empty( $data ) ){
			$query = $this->db->get_where( $this->_tbl_name , $data);
			if( $query->num_rows() > 0 ){
				$row = $query->row();
				$id = $row->id;
				if( !empty( $position ) ) $data['position'] = $position;
				$data['name'] = $name;
				$this->db->where('id', $id);
				$this->db->update( $this->_tbl_name , $data); 
			} else {
				// insert it
				if( !empty( $position ) ) $data['position'] = $position;
				$data['name'] = $name;
				//$data['date_added'] = date('Y-m-d H:i:s');
				$this->db->set('date_added', 'NOW()', FALSE);
				$this->db->insert( $this->_tbl_name , $data);
				$id = $this->db->insert_id(); 
			}	
		}
		
		return $id;
	}
	
	public function delete_question()
	{
		$question_id = $this->input->post('question_id');
		
		if( !empty( $question_id ) ) {
			return $this->db->delete( $this->_tbl_name, array( 'id' => $question_id ) );
		} else {
			$exam_id = $this->input->post('exam_id');
			$position = $this->input->post('position');
			if( !empty( $exam_id ) && !empty( $position ) ){
				return $this->db->delete( $this->_tbl_name, array( 'exam_id' => $exam_id, 'position' => $position ) );
			}
		}
		
		return FALSE;
	}
	
	
	public function reposition_question()
	{
		$question_id = $this->input->post('question_id');
		$new_position = $this->input->post('new_position');
		
		$data = NULL;
		$params = NULL;
		if( !empty( $new_position ) ) {
			$data = array( 'position' => $new_position );
		}
		
		if( !empty( $question_id ) ){
			$params = array( 'id' => $question_id );
		} else {
			$exam_id = $this->input->post('exam_id');
			$old_position = $this->input->post('old_position');
			if( !empty( $exam_id ) && !empty( $old_position ) ){
				$params = array( 'exam_id' => $exam_id, 'position' => $old_position );
			}
		}
		
		if( !empty( $data ) && !empty( $params ) ){
			return $this->db->update( $this->_tbl_name , $data, $params);
		}
		
		return FALSE;
	}
}
	