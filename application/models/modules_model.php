<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modules_model extends CI_Model {
	
	private $_tbl_name = 'exams';
	
	public function exam_detail( $id = NULL )
	{
		if( empty( $id ) ){
			$id = $this->input->post('id');
		}
		
		if( !empty( $id ) ){
			$query = $this->db->get_where( $this->_tbl_name , array('id' => $id));
			if( $query->num_rows() > 0 ){
				return $query->row();
			}
		}
		
		return FALSE;
	}
	
	public function delete_exam()
	{
		$action = $this->input->post('action');
		$id = $this->input->post('id');
		
		if( $action == 'delete' && !empty( $id ) ) {
			$this->db->delete( $this->_tbl_name , array('id' => $id)); 
			$this->session->set_userdata('delete', $id );
		}
	}
	
	public function save_new()
	{
		$id = 0;
		
		$action = $this->input->post('action');
		$name = $this->input->post('name');
		$description = $this->input->post('description');
		$time_allocation = $this->input->post('time_allocation');
		$passing_grade = $this->input->post('passing_grade');
		
		if( $action == 'save_new' && !empty( $name ) ) {
			
			$id = $this->input->post('id');
			
			$data = array(
				'name'			=> $name,
				'description'	=> $description,
				'time_allocation'	=> $time_allocation,
				'passing_grade'	=> $passing_grade,
				'added_by'		=> $this->session->userdata('user_id')
			);
			
			if( empty( $id ) ){
				$data['date_added'] = date('Y-m-d H:i:s');
				$this->db->insert( $this->_tbl_name , $data );
				$id = $this->db->insert_id();
				$this->session->set_userdata('new_exam', $id );
			} else {
				$this->db->where('id', $id);
				$this->db->update( $this->_tbl_name , $data );
				$this->session->set_userdata('edit_exam', $id );
			}
		}
		
		return $id;
	}
	
	public function check_exam_name( $name = NULL )
	{
		if( empty( $name ) ){
			$name = $this->input->post('name');
		}
		
		$id = $this->input->post('id');
		if( empty($id) ) $query = $this->db->get_where($this->_tbl_name, array( 'name' => $name ));
		else $query = $this->db->get_where($this->_tbl_name, array( 'name' => $name, 'id !=' => $id ));
		
		if ( $query->num_rows() > 0 ) return TRUE;
		return FALSE;
	}
	
	public function get_all()
	{
		$query =  $this->db->select( 'id, name, description, time_allocation, passing_grade' )->from( $this->_tbl_name )->order_by('name', 'asc')->get();
		if( $query->num_rows() > 0 ){
			return $query->result();
		} else {
			return FALSE;
		}
	}

	public function get_exam_name( $id = 0 )
	{
		
		$result = '';
		
		$query =  $this->db->select( 'name' )->from( $this->_tbl_name )->where('id', $id)->get();
		
		if( $query->num_rows() > 0 ){
			$row = $query->row();
			$result = $row->code;
		}
		
		return $result;
	}
	
	public function generate_random_string($length = 10) {
    	
    	$characters = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    	$charactersLength = strlen($characters);
    	$randomString = '';
    	
    	for ($i = 0; $i < $length; $i++) {
    		$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		
		return $randomString;
		
	}
}