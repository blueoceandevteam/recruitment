<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Applicants_model extends CI_Model {
	
	private $_tbl_name = 'applicants';
	private $_target_dir = 'uploads/resumes/';
	
	public function applicant_detail( $id = NULL )
	{
		if( empty( $id ) ){
			$id = $this->input->post('id');
		}
		
		if( !empty( $id ) ){
			$query = $this->db->get_where( $this->_tbl_name , array('id' => $id));
			if( $query->num_rows() > 0 ){
				return $query->row();
			}
		}
		
		return FALSE;
	}
	
	public function delete_applicant()
	{
		$action = $this->input->post('action');
		$id = $this->input->post('id');
		
		if( $action == 'delete' && !empty( $id ) ) {
			// $this->db->delete( $this->_tbl_name , array('id' => $id));
			$data['deleted'] = 1;
			$this->db->where('id', $id);
			$this->db->update( $this->_tbl_name , $data );
			
			$this->session->set_userdata('delete', $id );
		}
	}
	
	private function _process_resume( $id = 0, $first_name = NULL, $last_name = NULL )
	{
		$result = NULL;
		
		$file = isset( $_FILES['resume'] ) ? $_FILES['resume'] : NULL;
		
		if( !empty( $file ) && !empty( $id )){
			
			$file_name = $id.'-';
			$file_name .= !empty( $last_name ) ? strtolower(trim($last_name)) . '-' : '';
			$file_name .= !empty( $first_name ) ? strtolower(trim($first_name)) : '';
			
			$array = explode('.', $file['name'] );
			$ext = $array[ count( $array ) - 1 ];
			$file_name = str_replace(' ', '-', $file_name);
			$file_name .= '.'.$ext;
			
			$target_path = $_SERVER['DOCUMENT_ROOT'];
			$target_path .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
			$target_path .= $this->_target_dir;
			$target_path .= $file_name;
			
			if( move_uploaded_file($file['tmp_name'], $target_path) ){
				$result = $file_name;
			}
		} 
		
		return $result;
	}
	
	public function save_new()
	{
		$action = $this->input->post('action');
		$cat_id = $this->input->post('cat_id');
		$code = $this->input->post('code');
		$passcode = $this->input->post('passcode');
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		$phone_number = $this->input->post('phone_number');
		
		if( $action == 'save_new' && !empty( $cat_id ) && !empty( $code ) && !empty( $passcode ) && !empty( $first_name ) && !empty( $last_name ) ) {
			
			$id = $this->input->post('id');
			
			$data = array(
				'cat_id'		=> $cat_id,
				'code'			=> $code,
				'passcode'		=> $passcode,
				'first_name'	=> $first_name,
				'last_name'		=> $last_name,
				'phone_number'	=> $phone_number
			);			
			
			if( empty( $id ) ){
					
				$data['added_by'] = $this->session->userdata('user_id');
				$data['date_added'] = date('Y-m-d H:i:s');
				$this->db->insert( $this->_tbl_name , $data );
				$id = $this->db->insert_id();
				$this->session->set_userdata('new_applicant', $id );
					
			} else {
				
				$data['modified_by'] = $this->session->userdata('user_id');
				$this->db->where('id', $id);
				$this->db->update( $this->_tbl_name , $data );
				$this->session->set_userdata('edit_applicant', $id );
				
			}
			
			$resume = self::_process_resume( $id, $first_name, $last_name );
			
			if( !empty( $resume ) ){
				$data = array( 'resume' => $resume );
				$data['modified_by'] = $this->session->userdata('user_id');
				$this->db->where('id', $id);
				$this->db->update( $this->_tbl_name , $data );
				$this->session->set_userdata( 'edit_applicant', $id );
			}
			
			
		}
	}
	
	public function check_category_title( $title = NULL )
	{
		if( empty( $title ) ){
			$title = $this->input->post('title');
		}
		
		$id = $this->input->post('id');
		if( empty($id) ) $query = $this->db->get_where($this->_tbl_name, array( 'title' => $title ));
		else $query = $this->db->get_where($this->_tbl_name, array( 'title' => $title, 'id !=' => $id ));
		
		if ( $query->num_rows() > 0 ) return TRUE;
		return FALSE;
	}
	
	public function check_category_code( $code = NULL )
	{
		if( empty( $code ) ){
			$code = $this->input->post('code');
		}
		
		$id = $this->input->post('id');
		if( empty($id) ) $query = $this->db->get_where($this->_tbl_name, array( 'code' => $code ));
		else $query = $this->db->get_where($this->_tbl_name, array( 'code' => $code, 'id !=' => $id ));
		
		if ( $query->num_rows() > 0 ) return TRUE;
		return FALSE;
	}
	
	public function get_all()
	{
		$query =  $this->db->select( 'id, cat_id, code, passcode, first_name, last_name, phone_number' )->from( $this->_tbl_name )->where('deleted', 0)->order_by('date_added', 'desc')->get();
		if( $query->num_rows() > 0 ){
			return $query->result();
		} else {
			return FALSE;
		}
	}
	
	public function get_total_per_category( $cat_id = 0 )
	{
		$query =  $this->db->select( 'id' )->from( $this->_tbl_name )->get();
		
		if( !empty( $cat_id ) ){
			$query =  $this->db->select( 'id' )->from( $this->_tbl_name )->where( 'cat_id', $cat_id )->get();
		}
		
		return $query->num_rows();
	}
	
	public function check_applicant_code( $category_id = 0, $code = NULL )
	{
		$result = FALSE;
		
		if( !empty( $category_id ) && !empty( $code ) ){
			$query =  $this->db->select( 'id' )->from( $this->_tbl_name )->where( array('cat_id' => $category_id, 'code' => $code) )->get();
			if ( $query->num_rows() ) $result = TRUE;
		}
		
		return $result;
	}
	
	public function check_first_last_names( $first_name = NULL, $last_name = NULL )
	{
		$result = FALSE;
		if( empty( $first_name ) ) $first_name = $this->input->post('first_name');
		if( empty( $last_name ) ) $last_name = $this->input->post('last_name');
		
		if( !empty( $first_name ) && !empty( $last_name ) ) {
			$params = array( 'first_name' => $first_name, 'last_name' => $last_name );
			$query = $this->db->get_where( $this->_tbl_name , $params );
			if( $query->num_rows() > 0 ) $result = TRUE;
		}
		
		return $result;
	}
	
	public function check_applicant_auth( $code, $passcode ){

		$query =  $this->db->select( 'id' )
					   ->from( $this->_tbl_name )
					   ->where( array('code' => $code, 'passcode' => $passcode) )
					   ->get();
		
		if( $query->num_rows() > 0 ):
			$row = $query->row_array();
			return $row['id'];
		endif;
		return false;
		
	}
	
}