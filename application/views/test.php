<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sample</title>
    <link href="<?= base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?= base_url('assets/js/html5shiv.min.js'); ?>"></script>
      <script src="<?= base_url('assets/js/respond.min.js'); ?>"></script>
    <![endif]-->
  </head>
  <body class="skin-blue">
    <?php
    
    $my_list_ids = array();
	
	if( !empty( $mylist ) ){
		foreach ($mylist as $val) {
			$my_list_ids[] = $val->list_id;
		}
	}
    
    if( !empty( $lists ) ){
    	foreach ($lists as $list) {
			echo 'ID:'.$list->id.' <br />Name: '.$list->name.'<br />';
			if( !in_array( $list->id, $my_list_ids ) ){
				?><button type="button" class="btn addRow" onclick="addRow(<?= $list->id ?>)" id="add_<?php echo $list->id;?>" >Add</button><button type="button" id="del_<?php echo $list->id;?>" class="btn deleteRow" style="display: none">Remove</button><?php
			} else {
				?><button type="button" class="btn addRow" onclick="addRow(<?= $list->id ?>)" id="add_<?php echo $list->id;?>" style="display: none">Add</button><button id="del_<?php echo $list->id;?>" class="btn deleteRow" type="button">Remove</button><?php
			}
			echo "<hr />";
		}
    }
    ?>
    <script src="<?= base_url('assets/js/jquery-1.11.2.min.js'); ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script>
    	function addRow(id){
    		$.post('<?= site_url('test/add')?>', { id: id }).done(function (res){
    			if( res == 'true' ){
    				$('#add_'+id).hide();
    				$('#del_'+id).show();
    			}
    		});
    	}
    </script>
  </body>
</html>