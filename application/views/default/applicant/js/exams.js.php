<script type="text/javascript">
	$(document).ready(function(){
		$('.exam_type .question').hide();
		$('.exam_type .question:first-child').show();
		
		$('.final_answer').click(function(){
			
			var parent_form = $(this).closest('form');

			var data = parent_form.serializeArray();
				
			var final_answer = confirm("Is this your final answer?")
			
				
	if (final_answer) {	
				
			$.post('<?= site_url('ajax/saveanswer'); ?>', { quest_id: parseInt(data[1].value), answer : data[0].value } ).done( function(res) {
				if( res == 'success' ) {
					parent_form.next().fadeIn();
					if(parent_form.next().length > 0) {
						parent_form.remove();
					} else {
						window.location.href= "";
					}
					//parent_form.next().show();
				} else {
					alert('Unable to insert data');
				}
			});
		} //end confirmation box

		});

	});
</script>