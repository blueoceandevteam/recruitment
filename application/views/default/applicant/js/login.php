<script type="text/javascript">
	$(document).ready(function(){
		
		$('input[type="text"]').keypress(function(event){
			if( event.which === 13 && $('#username').val() != '' && $('#password').val() ){
				$('#submitBtn').click();
			}
		});
		
		$('input[type="password"]').keypress(function(event){
			if( event.which === 13 && $('#username').val() != '' && $('#password').val() ){
				$('#submitBtn').click();
			}
		});
		
		$('.form-control').keyup(function(){
			$('#errorMessage').hide();
			var res = true;
			var tmp;
			$('.form-control').each(function(){
				tmp = $.trim( $(this).val() ) ;
				if( tmp == '' ){
					res = false;
				}
			});
			if( res == true ) {
				$('#submitBtn').removeAttr('disabled');
				
			} else {
				$('#submitBtn').attr('disabled', 'disabled'	);
			}
			
		});
		
		$('#submitBtn').click(function(){
			// check if valid username and password combi
			$.post('<?= site_url('ajax/authenticate/applicant'); ?>', { username: $('#username').val(), password : $('#password').val() } ).done( function(res) {
				if( res == 'success' ) {
					// set the sessions
					window.location.href= "<?= site_url('applicant/index'); ?>";
				} else {
					// show warning
					$('.form-group').attr('class', 'form-group has-error');
					$('#errorMessage').show();
				}
			});
	
		});
		
		
	});
</script>