<header class="header">
    <a href="#" class="logo">
        <!-- Add the class icon to your logo image or logo icon to add the margining -->
        Blue Ocean BPO
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <!--li class="label-primary">
                    <a href="<?= site_url('admin/profile') ?>">
                        <i class="fa fa-key"></i> <strong>Edit Profile</strong>
                    </a>
                </li-->
                <li class="label-warning">
                    <a href="<?= site_url('applicant/logout') ?>">
                        <i class="fa fa-sign-out"></i> <strong>Log Out</strong>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>