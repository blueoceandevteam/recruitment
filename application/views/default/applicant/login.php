<div class="container">
	<div class="col-sm-4 col-sm-offset-4" style="margin-top: 50px">
		<form action="<?= base_url('admin/index')?>" method="post" role="form">
			<h2>Enter Your Id</h2>
			<div class="alert alert-danger" style="display: none" id="errorMessage">Invalid ID / security code</div>
			<div class="form-group">
		    	<label for="username" class="sr-only">Applicant ID</label>
		    	<input type="text" id="username" name="username" class="form-control" placeholder="Applicant ID" required autofocus>	
		    </div>
		    <div class="form-group">
		    	<label for="password" class="sr-only">Security Code</label>
		    	<input type="password" name="password" id="password" class="form-control" placeholder="Security Code" required>
		    </div>
		    <div class="form-actions">
		    	<button id="submitBtn" class="btn btn-lg btn-success btn-block" type="button" disabled>Login</button>
		    </div>
	  </form>	
	</div>	
</div>