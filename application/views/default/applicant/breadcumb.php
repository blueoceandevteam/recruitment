<ol class="breadcrumb">
    <li><a href="<?= site_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <?php
    if( !empty( $items ) ){
    	foreach ($items as $href => $item) {
			?>
	<li><a href="<?= ($href != '#') ? site_url('admin/'.$href) : $href;?>"<?= ($href == '#') ? ' class="active"' : '' ?>><?= $item; ?></a></li>	
			<?php
		}
    }
    ?>
</ol>