<?php #Exams ?>


		<div class="col-md-7 col-md-offset-2">
 	 		<div class="panel panel-primary">
  				<div class="panel-heading">
    				<h3 class="panel-title">Select Test</h3>
  				</div>
  				<div class="panel-body">
    				Test description here...
  				</div>
			</div>

		<div class="list-group">
  		<?php if(!empty($exam_list)): foreach($exam_list as $list):
  			$row = $list->row_array();?>
		
 			<a href="<?php echo base_url() ?>applicant/start/<?php echo $row['id']?>" class="list-group-item">
    			<h4 class="list-group-item-heading"><?php echo $row['name']?></h4>
    			<p class="list-group-item-text"><?php echo $row['description']?></p>
   			</a>
		<?php endforeach; endif; ?>
		</div>
  	</div>
