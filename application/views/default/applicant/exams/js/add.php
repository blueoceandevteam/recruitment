<script type="text/javascript">
	$(document).ready(function(){
		
		$('input[type="text"]').keypress(function(event){
			if( event.which == 13 ){
				if( ! $('#submitBtn').is(':disabled') ) $('#submitBtn').click();
			}
		});
		
		$('#code').keyup(function(){
			$(this).val($(this).val().toUpperCase());
		});
		
		$('#title').keyup(function(){
			$('#errorMessage').hide();
			var res = true;
			var tmp;
			tmp = $.trim( $(this).val() ) ;
			$(this).parent().attr('class', 'form-group');
			$('#code').removeAttr('readonly');
			$('#titleAria').hide();
			if( tmp == '' ){
				$('#submitBtn').attr('disabled', 'disabled'	);
				$(this).parent().attr('class', 'form-group has-feedback has-error');
				$('#titleAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
				$('#titleStatus').html('(error)');
				$('#titleAria').show();
			} else {
				// check database
				$.post('<?= site_url('ajax/check_module_title'); ?>', {title : tmp, id: $('#moduleID').val() }).done(function( result ){
					if( result == 'success' ){
						$('#field').html('title');
						$('#code').attr('readonly', 'readonly');
						$('#errorMessage').show();
						$('#submitBtn').attr('disabled', 'disabled'	);
						$('#title').parent().attr('class', 'form-group has-feedback has-error');
						$('#titleAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
						$('#titleStatus').html('(error)');
						$('#titleAria').show();
					} else {
						tmp = $.trim( $('#code').val() ) ;
						if( tmp == '' ) res = false;
						$('#title').parent().attr('class', 'form-group has-feedback has-success');
						$('#titleAria').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
						$('#titleStatus').html('(success)');
						$('#titleAria').show();
						if( res == true ) {
							$('#submitBtn').removeAttr('disabled');
						} else {
							$('#submitBtn').attr('disabled', 'disabled'	);
						}
					}
				});
			}
		});
		
		$('#code').keyup(function(){
			$('#errorMessage').hide();
			var res = true;
			var tmp;
			tmp = $.trim( $(this).val() ) ;
			$(this).parent().attr('class', 'form-group');
			$('#title').removeAttr('readonly');
			$('#codeAria').hide();
			if( tmp == '' ){
				$('#submitBtn').attr('disabled', 'disabled'	);
				$(this).parent().attr('class', 'form-group has-feedback has-error');
				$('#codeAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
				$('#codeStatus').html('(error)');
				$('#codeAria').show();
			} else {
				// check database
				$.post('<?= site_url('ajax/check_module_code'); ?>', {code : tmp, id: $('#moduleID').val() }).done(function( result ){
					if( result == 'success' ){
						$('#field').html('code');
						$('#title').attr('readonly', 'readonly');
						$('#errorMessage').show();
						$('#submitBtn').attr('disabled', 'disabled'	);
						$('#code').parent().attr('class', 'form-group has-feedback has-error');
						$(this).parent().attr('class', 'form-group has-feedback has-error');
						$('#codeAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
						$('#codeStatus').html('(error)');
						$('#codeAria').show();
					} else {
						tmp = $.trim( $('#title').val() ) ;
						if( tmp == '' ) res = false;
						$('#code').parent().attr('class', 'form-group has-feedback has-success');
						$(this).parent().attr('class', 'form-group has-feedback has-success');
						$('#codeAria').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
						$('#codeStatus').html('(success)');
						$('#codeAria').show();
						if( res == true ) {
							$('#submitBtn').removeAttr('disabled');
						} else {
							$('#submitBtn').attr('disabled', 'disabled'	);
						}
					}
				});
			}
		});
		
		$('#submitBtn').click(function(){
			$('#moduleForm').submit();
		});
		
	});
</script>