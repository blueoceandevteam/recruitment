<?php # ?>

<div class="col-md-12 col-md-offset-2" style="margin-bottom:20px;">
	<div class="col-md-3 col-md-offset-8">
<?php if(!empty($questions)): ?>
<script type="application/javascript">
var exam_time = new Countdown({
					time: 15, 
					width:150, 
					height:80, 
					onComplete	: function(){ alert('your time has expired!'); },
					rangeHi:"minute"	// <- no comma on last item!
			});

</script>
<?php endif; ?>
	</div>
</div>

<div class="col-md-7 col-md-offset-2 exam_type">
	<?php #var_dump($questions); ?>
	<?php if(!empty($questions)): foreach($questions as $question): ?>
	<form class="form-horizontal question">
		
		<div class="panel panel-primary">
  			<div class="panel-heading">
    			<h3 class="panel-title"><?php echo $question['question']['name']?></h3>
  			</div>
  			<div class="panel-body">
  				<?php foreach($question['choices']->result_array() as $vchoice): ?>
  					<div class="btn-group" data-toggle="buttons">
  						<label class="btn btn-primary active">
    						<input type="radio" data="<?php echo $vchoice['question_id']?>"  name="question" id="option1" value="<?php echo $vchoice['name']?>" autocomplete="off"> <?php echo $vchoice['name']?>
  						</label>
  					</div>
    			<?php endforeach; ?>
  			</div>
		
			<div class="panel-body">
				<input type="hidden" name="question_id" value="<?php echo $question['question']['id']; ?>" />
				<input type="button" name="next" class="btn btn-primary pull-right final_answer" value="Next" />
				
			</div>
		</div>
	</form>
	<?php endforeach; ?>
	<?php else: ?>
		<div class="panel panel-primary">
  			<div class="panel-heading">
    			<h3 class="panel-title">Please proceed to the next exam!</h3>
  			</div>
  			<div class="panel-body">
  				<p>You have completed this exam already . </p>
  				<a href="<?= site_url('applicant/start/3'); ?>">
					<input type="button" name="next" class="btn btn-primary pull-right" value="Continue..." />
  				</a>
  			</div>
		</div>
 	<?php endif; ?>

	
</div>


