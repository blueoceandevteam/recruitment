<div class="container">
	<div class="col-sm-4 col-sm-offset-4" style="margin-top: 50px">
		<form action="<?= base_url('admin/index')?>" method="post" role="form">
			<h2>Please sign in</h2>
			<div class="alert alert-danger" style="display: none" id="errorMessage">Invalid username/password</div>
			<div class="form-group">
		    	<label for="username" class="sr-only">Username</label>
		    	<input type="text" id="username" name="username" class="form-control" placeholder="Username" required autofocus>	
		    </div>
		    <div class="form-group">
		    	<label for="password" class="sr-only">Password</label>
		    	<input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
		    </div>
		    <div class="checkbox">
		      <label>
		        <input type="checkbox" value="remember-me"> Remember me
		      </label>
		    </div>
		    <div class="form-actions">
		    	<button id="submitBtn" class="btn btn-lg btn-success btn-block" type="button" disabled>Sign in</button>
		    </div>
	  </form>	
	</div>	
</div>