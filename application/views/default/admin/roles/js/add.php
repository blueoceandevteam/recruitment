<script type="text/javascript">

	$(document).ready(function(){
		
		$('input[type="text"]').keypress(function(event){
			if( event.which == 13 ){
				if( ! $('#submitBtn').is(':disabled') ) $('#submitBtn').click();
			}
		});
		
		
		$('#description').keyup(function(){
			var myCode = $('#code').val();
			myCode = $.trim( myCode ) ;
			if( myCode != '' ){
				$('#submitBtn').removeAttr('disabled');
			}
		});
		$('#code').keyup(function(){
			$('#errorMessage').hide();
			var res = true;
			var tmp;
			tmp = $.trim( $(this).val() ) ;
			$(this).parent().attr('class', 'form-group');
			$('#codeAria').hide();
			if( tmp == '' ){
				$('#submitBtn').attr('disabled', 'disabled'	);
				$(this).parent().attr('class', 'form-group has-feedback has-error');
				$('#codeAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
				$('#codeStatus').html('(error)');
				$('#codeAria').show();
			} else {
				// check database
				$.post('<?= site_url('ajax/check_role_code'); ?>', {code : tmp, id: $('#roleID').val() }).done(function( result ){
					if( result == 'success' ){
						$('#field').html('code');
						$('#title').attr('readonly', 'readonly');
						$('#errorMessage').show();
						$('#submitBtn').attr('disabled', 'disabled'	);
						$('#code').parent().attr('class', 'form-group has-feedback has-error');
						$(this).parent().attr('class', 'form-group has-feedback has-error');
						$('#codeAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
						$('#codeStatus').html('(error)');
						$('#codeAria').show();
					} else {
						$('#code').parent().attr('class', 'form-group has-feedback has-success');
						$(this).parent().attr('class', 'form-group has-feedback has-success');
						$('#codeAria').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
						$('#codeStatus').html('(success)');
						$('#codeAria').show();
						if( res == true ) {
							$('#submitBtn').removeAttr('disabled');
						} else {
							$('#submitBtn').attr('disabled', 'disabled'	);
						}
					}
				});
			}
		});
		
		$('#submitBtn').click(function(){
			$('#roleForm').submit();
		});
		
	});
</script>