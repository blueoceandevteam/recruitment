<div class="box box-primary">
	<div class="box-header">
		<?php
		
		$temp_id = 0;
		
		$flash_added = $this->session->userdata('new_role');
		$temp_id = !empty( $flash_added ) ? $flash_added : $temp_id;
		$this->session->set_userdata('new_role', NULL);
		if( !empty( $flash_added ) ){
			?>
		<div class="pull-left alert alert-success" style="margin: 10px" id="alertAction">Role Setting successfully added!</div>
			<?php
		}
		
		$flash_added = $this->session->userdata('edit_role');
		$temp_id = !empty( $flash_added ) ? $flash_added : $temp_id;
		$this->session->set_userdata('edit_role', NULL);
		if( !empty( $flash_added ) ){
			?>
		<div class="pull-left alert alert-success" style="margin: 10px" id="alertAction">Role Setting successfully modified!</div>
			<?php
		}
		
		$flash_added = $this->session->userdata('delete');
		$temp_id = !empty( $flash_added ) ? $flash_added : $temp_id;
		$this->session->set_userdata('delete', NULL);
		if( !empty( $flash_added ) ){
			?>
		<div class="pull-left alert alert-danger" style="margin: 10px" id="alertAction">Role Setting successfully deleted!</div>
			<?php
		}
		?>
		<button class="btn btn-success pull-right btn-lg" style="margin: 10px;" type="button" id="addRole">
			<i class="fa fa-plus-square-o"></i> Add New
		</button>
	</div>
	<div class="box-body table-responsive">
		<?php if( !empty( $roles ) ) {
			?>
		<table class="table" id="datatable" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Code</th>
					<th>Description</th>
					<th>Actions</th>
				</tr>
			</thead>
			<?php
			if( count( $roles ) > 20 ){
				?>
			<tfoot>
				<tr>
					<th>Code</th>
					<th>Description</th>
					<th>Actions</th>
				</tr>
			</tfoot>
				<?php
			}
			?>
			
			<tbody>
			<?php
				foreach( $roles as $role )
				{
					?>
			<tr<?= $role->id == $temp_id ? ' class="success"' : '' ?>>
				<td><?= $role->code ?></td>
				<td><?= $role->description ?></td>
				<td>
					<button class="btn btn-primary btn-sm" type="button" onclick="editRole(<?= $role->id ?>)">
						<i class="fa fa-pencil"></i> Edit
					</button>
					<button class="btn btn-danger btn-sm" type="button" onclick="deleteRole(<?= $role->id ?>)">
						<i class="fa fa-trash"></i> Delete
					</button>
				</td>
			</tr>
					<?php
				}
			?>
			</tbody>
		</table>
			<?php
		} else {
			echo 'no records yet...';
		} ?>
		
	</div>
</div>
<form style="display: none" id="hiddenForm" method="POST">
	<input type="hidden" name="action" value="" />
	<input type="hidden" name="id" value="" />
</form>