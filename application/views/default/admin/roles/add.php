<div class="col-md-6 col-md-offset-3 ">
	<div class="box box-primary">
		<div class="box-body table-responsive">
			<form role="form" method="post" id="roleForm">
				<div class="box-header">
					<div class="alert alert-danger" style="display: none" id="errorMessage">Role Setting <span id="field"></span> already exists!</div>
				</div>
				<div class="box-body">
					<div class="form-group">
						<label for="code">Code</label>
						<input type="text" class="form-control" name="code" id="code" placeholder="role-code" required aria-describedby="codeStatus" value="<?= !empty( $role->code ) ? $role->code : '' ?>" />
						<span id="codeAria" aria-hidden="true" style="display: none"></span>
						<span id="codeStatus" class="sr-only"></span>
					</div>
					<div class="form-group">
						<label for="description">Description</label>
						<textarea class="form-control" name="description" id="description"><?= !empty( $role->description ) ? $role->description : '' ?></textarea>
					</div>
				</div>
				<div class="box-footer">
                    <button type="button" class="btn" onclick="window.location.href='<?= site_url('admin/roles') ?>'">Cancel</button>&nbsp;
                    <button type="button" class="btn btn-primary" disabled="disabled" id="submitBtn"><?= !empty( $role->id ) ? 'Edit' : 'Add' ?> Role Setting</button>
                </div>
                <input type="hidden" value="save_new" name="action" />
                <input type="hidden" value="<?= !empty( $role->id ) ? $role->id : '' ?>" name="id" id="roleID" />
			</form>
		</div>
	</div>
</div>
