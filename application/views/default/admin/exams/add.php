<div class="col-md-10 col-md-offset-1">
	<div class="box box-primary">
		<div class="box-body table-responsive">
			<form role="form" method="post" id="examForm">
				<div class="box-header">
					<div class="alert alert-danger" style="display: none" id="errorMessage">Exam <span id="field"></span> already exists!</div>
				</div>
				<div class="box-body">
					<div class="form-group<?= !empty( $exam->id ) ? ' has-feedback has-success':'' ?>">
						<label for="name">Name</label>
						<input type="text" class="form-control" name="name" id="name" placeholder="Name" required autofocus aria-describedby="nameStatus" value="<?= !empty( $exam->name ) ? $exam->name : '' ?>" />
						<span id="nameAria" aria-hidden="true" style="display: none"></span>
						<span id="nameStatus" class="sr-only"></span>
					</div>
					<div class="form-group">
						<label for="description">Description</label>
						<input type="text" class="form-control" name="description" id="description" placeholder="Description" required aria-describedby="descriptionStatus" value="<?= !empty( $exam->description ) ? $exam->description : '' ?>" />
						<span id="descriptionAria" aria-hidden="true" style="display: none"></span>
						<span id="descriptionStatus" class="sr-only"></span>
					</div>
					<div class="form-group<?= !empty( $exam->id ) ? ' has-feedback has-success':'' ?>">
						<label for="time_allocation">Time Limit (minutes)</label>
						<input type="number" min="0" class="form-control" name="time_allocation" id="time_allocation" required autofocus aria-describedby="timeLimitStatus" value="<?= !empty( $exam->time_allocation ) ? $exam->time_allocation : '' ?>" />
						<span id="timeLimitAria" aria-hidden="true" style="display: none"></span>
						<span id="timeLimitStatus" class="sr-only"></span>
					</div>
					<div class="box-header">
						<div class="alert alert-danger" style="display: none" id="timeMessage">Time must be a positive number!</div>
					</div>
					<div class="form-group<?= !empty( $exam->id ) ? ' has-feedback has-success':'' ?>">
						<label for="passing_grade">Passing Grade (%)</label>
						<input type="number" min="0" max="100" class="form-control" name="passing_grade" id="passing_grade" required autofocus aria-describedby="passingGradeStatus" value="<?= !empty( $exam->passing_grade ) ? $exam->passing_grade : '' ?>" />
						<span id="passingGradeAria" aria-hidden="true" style="display: none"></span>
						<span id="passingGradeStatus" class="sr-only"></span>
					</div>
					<div class="box-header">
						<div class="alert alert-danger" style="display: none" id="passingMessage"></div>
					</div>
				</div>
				<div class="box-body" id="divQuestion"></div>
				<div class="box-footer col-md-12">
                    <button type="button" class="btn btn-danger btn-lg" onclick="window.location.href='<?= site_url('admin/exams') ?>'"><i class="fa fa-undo"></i>&nbsp;Cancel</button>&nbsp;
                    <button type="button" class="btn btn-primary btn-lg"<?= !empty( $exam->id) ? '':' disabled="disabled"' ?> id="submitBtn"><i class="fa fa-floppy-o"></i>&nbsp;<?= !empty( $exam->id ) ? 'Save Changes' : 'Add Exam' ?></button>
                    <button type="button" class="btn btn-success btn-lg" id="addQuestion"<?= !empty( $exam->id) ? '':' disabled="disabled"' ?>><i class="fa fa-plus-circle"></i>&nbsp;Add Question</button>
                </div>
                <input type="hidden" value="save_new" name="action" />
                <input type="hidden" value="<?= !empty( $exam->id ) ? $exam->id : '' ?>" name="id" id="examID" />
			</form>
		</div>
	</div>
</div>
