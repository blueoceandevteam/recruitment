<div class="box box-primary">
	<div class="box-header">
		<?php
		
		$temp_id = 0;
		
		$flash_added = $this->session->userdata('new_exam');
		$temp_id = !empty( $flash_added ) ? $flash_added : $temp_id;
		$this->session->set_userdata('new_exam', NULL);
		if( !empty( $flash_added ) ){
			?>
		<div class="pull-left alert alert-success" style="margin: 10px" id="alertAction">Exam successfully added!</div>
			<?php
		}
		
		$flash_added = $this->session->userdata('edit_exam');
		$temp_id = !empty( $flash_added ) ? $flash_added : $temp_id;
		$this->session->set_userdata('edit_exam', NULL);
		if( !empty( $flash_added ) ){
			?>
		<div class="pull-left alert alert-success" style="margin: 10px" id="alertAction">Exam successfully modified!</div>
			<?php
		}
		
		$flash_added = $this->session->userdata('delete');
		$temp_id = !empty( $flash_added ) ? $flash_added : $temp_id;
		$this->session->set_userdata('delete', NULL);
		if( !empty( $flash_added ) ){
			?>
		<div class="pull-left alert alert-danger" style="margin: 10px" id="alertAction">Exam successfully deleted!</div>
			<?php
		}
		?>
		<button class="btn btn-success pull-right btn-lg" style="margin: 10px;" type="button" id="addExam">
			<i class="fa fa-plus-square-o"></i> Add New
		</button>
	</div>
	<div class="box-body table-responsive">
		<?php if( !empty( $exams ) ) {
			?>
		<table class="table" id="datatable" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Name</th>
					<th>Time Limit</th>
					<th>Passing Grade</th>
					<th>Description</th>
					<th>Actions</th>
				</tr>
			</thead>
			<?php
			if( count( $exams ) > 20 ){
				?>
			<tfoot>
				<tr>
					<th>Name</th>
					<th>Time Limit</th>
					<th>Passing Grade</th>
					<th>Description</th>
					<th>Actions</th>
				</tr>
			</tfoot>
				<?php
			}
			?>
			
			<tbody>
			<?php
				foreach( $exams as $exam )
				{
					?>
			<tr<?= $exam->id == $temp_id ? ' class="success"' : '' ?>>
				<td><?= $exam->name ?></td>
				<td><?= !empty($exam->time_allocation) ? $exam->time_allocation : '0' ?> minutes</td>
				<td><?= !empty($exam->passing_grade) ? $exam->passing_grade : '0' ?> %</td>
				<td><?= $exam->description ?></td>
				<td>
					<button class="btn btn-primary btn-sm" type="button" onclick="editExam(<?= $exam->id ?>)">
						<i class="fa fa-pencil"></i> Edit
					</button>
					<button class="btn btn-danger btn-sm" type="button" onclick="deleteExam(<?= $exam->id ?>)">
						<i class="fa fa-trash"></i> Delete
					</button>
				</td>
			</tr>
					<?php
				}
			?>
			</tbody>
		</table>
			<?php
		} else {
			echo 'no records yet...';
		} ?>
		
	</div>
</div>
<form style="display: none" id="hiddenForm" method="POST">
	<input type="hidden" name="action" value="" />
	<input type="hidden" name="id" value="" />
</form>