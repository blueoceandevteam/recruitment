<script src="<?= base_url('assets/js/tinymce/tinymce.min.js') ?>"></script>
<script src="<?= base_url('assets/js/tinymce/jquery.tinymce.min.js') ?>"></script>
<script type="text/javascript">
	
	tinymce.init({
	    selector: "textarea",
	    plugins: [
	        "advlist autolink lists link image charmap print preview anchor",
	        "searchreplace visualblocks code fullscreen",
	        "insertdatetime media table contextmenu paste"
	    ],
	    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
	    setup:	function(ed) {
	    			ed.on('keyup', function(e) {
	    				idVal = $(this).attr('id');
	    				
	    				if( idVal.indexOf('question_') >= 0 ){
	    					$.post( '<?= site_url('ajax/save_question') ?>', { question_id: idVal.replace('question_', ''), name : tinyMCE.activeEditor.getContent() } );
	    				} else if( idVal.indexOf('answers_') >= 0 ){
	    					idVal = idVal.replace('answers_', '');
	    					idVal = idVal.split('_');
	    					$.post( '<?= site_url('ajax/save_choice') ?>', { id: idVal[1], name : tinyMCE.activeEditor.getContent() } );
	    				}
	    			});
	    		}
	});

	var questionCounter = 0;
	var examID = <?= !empty( $exam->id ) ? $exam->id : 0 ?>;

	function checkFormStatus(){
		var timeLimitVal = $('#time_allocation').val();
		var passingGradeVal = $('#passing_grade').val();
		var nameVal = $('#name').val();
		nameVal = $.trim(nameVal);
		
		var res = true;
		
		if( passingGradeVal > 100 || passingGradeVal < 0 || timeLimitVal < 0 || timeLimitVal == '' || nameVal == '') res = false;
		
		if( res ){
			$('#submitBtn').removeAttr( 'disabled' );
			$('#addQuestion').removeAttr( 'disabled' );
		}
		
	}
	
	$(document).ready(function(){
		
		
		<?php
		$question_position = array();
		
		if( !empty ( $questions ) ) {
			foreach( $questions as $key => $question ){
				$question_position[ $question->id ] = $key + 1;
				?>
				questionCounter++;
				//var randomWord = 'divQuestion'+questionCounter;
				var randomWord = 'divQuestion<?=$question->id?>';
				var cntQuest = $('.divQuestion').length;
				if( cntQuest == 0 ){
					$('#divQuestion').append('<div class="box box-warning"><div class="box-header"><h3 class="box-title">QUESTIONS</h3></div><div class="questions"></div></div>');
				}
				
				$('.questions').append('<div id="'+randomWord+'" class="box-body divQuestion"></div>');
				var qHTML = '<div class="form-group fg" id="fg_<?= $question->id;?>">';
				qHTML += '<label for="question_<?= $question->id?>" class="questionLabel" id="questionLabel_<?= $question->id;?>">Question #<?= $question_position[ $question->id ];?></label>';
				qHTML += '<span class="delQuestion" id="delQuestion_<?= $question->id;?>" onclick="removeQuestion(<?= $question_position[ $question->id ];?>,<?= $question->id ?>)" style="float:right">x</span>';
				qHTML += '<textarea class="form-control questionInput textarea" id="question_<?= $question->id;?>" name="question_label[]"><?= addcslashes( htmlentities($question->name, ENT_QUOTES, 'UTF-8') ,"\\\'\"\n\r"); ?></textarea>';
				/*qHTML += '<input type="text" class="form-control questionInput" id="question_'+questionCounter+'" onkeyup="questionKeyup('+questionCounter+')" name="question_label[]" placeholder="Enter Question">';*/
				qHTML += '<div class="questionChoices" id="questionChoices_<?= $question->id;?>" style="display:none"></div>';
				qHTML += '<div id="btnAnswer_<?= $question->id;?>" class="btnAnswer col-md-12 text-right" style="padding-top:25px;"><button type="button" class="btn btn-info btn-sm addMultiple" onclick="addMultiple(<?= $question->id;?>)"><i class="fa fa-server"></i>&nbsp;Add answer choice</button>&nbsp;<button type="button" class="btn btn-warning btn-sm addText" onclick="addText(<?= $question->id;?>)"><i class="fa fa-file-text-o"></i>&nbsp;Add answer text</button></div>';
				qHTML += '</div>';
				$('#'+randomWord).append( qHTML );
				$('#fg_<?= $question->id;?>').focus();
			<?php		
			}
		}
				
		if( !empty( $choices ) ){
			foreach ($choices as $ckey => $choice) {
				if( $choice->type == 0 ){
					?>
					var totalChoices = $('#questionChoices_<?= $choice->question_id ?> .answerChoices').length;
					var choicesHtml = '<div class="answerChoices row" id="qc_<?= $choice->question_id ?>_<?= $choice->id ?>" style="padding-top:10px;"><label>'+ $('#fg_<?= $choice->question_id ?> .questionLabel').html() +' Choice '+ (totalChoices + 1) +':</label>';
					choicesHtml += '<span class="pull-right" onclick="deleteOption(<?= $choice->question_id ?>, <?= $choice->id ?>, \'multiple\')">x</span>';
					choicesHtml += '<br>';
					choicesHtml += '<input type="radio"<?= $choice->is_correct ? ' checked="checked"' : '' ?> name="answers_correct_<?= $choice->question_id ?>" value="<?= $choice->id ?>"> <label class="text-success"> Correct Answer?</label>';
					
					if( totalChoices == 0 ){
						$('#btnAnswer_<?= $choice->question_id ?> .addMultiple').html('<i class="fa fa-server"></i>&nbsp;Add another answer choice');
						$('#btnAnswer_<?= $choice->question_id ?> .addText').hide();
					}
					
					choicesHtml += '<div class="col-md-9 col-md-offset-3"><textarea class="textarea form-control optionText" name="answers_<?= $choice->question_id ?>[]" id="answers_<?= $choice->question_id ?>_<?= $choice->id ?>"><?= addcslashes( htmlentities($choice->name, ENT_QUOTES, 'UTF-8') ,"\\\'\"\n\r"); ?></textarea></div>';
					choicesHtml += '</div>';
					
					$('#questionChoices_<?= $choice->question_id?>').append( choicesHtml );
					$('#questionChoices_<?= $choice->question_id ?>').show();
					<?php
				} else {
					?>
					var totalChoices = $('#questionChoices_<?= $choice->question_id ?> .answerChoices').length;
					var choicesHtml = '<div class="answerChoices row" id="qc_<?= $choice->question_id ?>_<?= $choice->id ?>" style="padding-top:10px;"><label>'+ $('#fg_<?= $choice->question_id ?> .questionLabel').html() +' Answer:</label>';
					choicesHtml += '<span class="pull-right" onclick="deleteOption(<?= $choice->question_id ?>, <?= $choice->id ?>), \'text\'">x</span>';
					
					if( totalChoices >= 0 ){
						$('#btnAnswer_<?= $choice->question_id ?> .addText').html('<i class="fa fa-file-text-o"></i>&nbsp;Add another answer text');
						$('#btnAnswer_<?= $choice->question_id ?> .addMultiple').hide();
					}
					
					choicesHtml += '<div class="col-md-9 col-md-offset-3"><input type="text" class="form-control optionText" name="answers_<?= $choice->question_id ?>[]" id="answers_<?= $choice->question_id ?>_<?= $choice->id ?>" value="<?= $choice->name; ?>"></div>';
					choicesHtml += '</div>';
					
					$('#questionChoices_<?= $choice->question_id ?>').append( choicesHtml );
					$('#questionChoices_<?= $choice->question_id ?>').show();
					<?php
				}
			}
		}
		?>
		
		$('input[type="text"]').keypress(function(event){
			if( event.which == 13 ){
				if( ! $('#submitBtn').is(':disabled') ) $('#submitBtn').click();
			}
		});
		
		$('#time_allocation').keyup( function() {
			$('#timeMessage').hide();
			var res = true;
			var tmp;
			tmp = $.trim( $(this).val() ) ;
			$(this).parent().attr('class', 'form-group');
			$('#timeLimitAria').hide();
			if( tmp == '' || tmp < 1 ){
				$('#submitBtn').attr('disabled', 'disabled'	);
				$(this).parent().attr('class', 'form-group has-feedback has-error');
				$('#timeLimitAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
				$('#timeLimitStatus').html('(error)');
				$('#timeLimitAria').show();
				$('#timeMessage').show();
			} else {
				// check database
				$('#time_allocation').parent().attr('class', 'form-group has-feedback has-success');
				$('#timeLimitAria').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
				$('#timeLimitStatus').html('(success)');
				$('#timeLimitAria').show();
			}
			checkFormStatus();
		} );
		
		$('#passing_grade').keyup( function() {
			
			$('#passingMessage').hide();
			var res = true;
			var tmp;
			tmp = $.trim( $(this).val() ) ;
			$(this).parent().attr('class', 'form-group');
			$('#passingGradeAria').hide();
			
			if( tmp == '' || tmp < 1 || tmp > 100 ){
				$('#submitBtn').attr('disabled', 'disabled'	);
				$(this).parent().attr('class', 'form-group has-feedback has-error');
				$('#passingGradeAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
				$('#passingGradeStatus').html('(error)');
				$('#passingGradeAria').show();
				
				if( tmp == '' || tmp < 1 ) $('#passingMessage').html('Passing Grade must be greater than 0!');
				else $('#passingMessage').html('Passing Grade must be lesser than or equal to 100!');
				
				$('#passingMessage').show();
				$('#submitBtn').attr('disabled', 'disabled'	);
				$('#addQuestion').attr('disabled', 'disabled');
				
			} else {
				// check database
				$('#passing_grade').parent().attr('class', 'form-group has-feedback has-success');
				$('#passingGradeAria').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
				$('#passingGradeStatus').html('(success)');
				$('#passingGradeAria').show();
			}
			checkFormStatus();
		} );
		
		$('#name').keyup(function(){
			$('#errorMessage').hide();
			var res = true;
			var tmp;
			tmp = $.trim( $(this).val() ) ;
			$(this).parent().attr('class', 'form-group');
			$('#nameAria').hide();
			if( tmp == '' ){
				$('#submitBtn').attr('disabled', 'disabled'	);
				$(this).parent().attr('class', 'form-group has-feedback has-error');
				$('#nameAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
				$('#nameStatus').html('(error)');
				$('#nameAria').show();
			} else {
				// check database
				$.post('<?= site_url('ajax/check_exam_name'); ?>', {name : tmp, id: $('#examID').val() }).done(function( result ){
					if( result == 'success' ){
						$('#field').html('name');
						$('#errorMessage').show();
						$('#submitBtn').attr('disabled', 'disabled'	);
						$('#name').parent().attr('class', 'form-group has-feedback has-error');
						$('#nameAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
						$('#nameStatus').html('(error)');
						$('#nameAria').show();
					} else {
						$('#name').parent().attr('class', 'form-group has-feedback has-success');
						$('#nameAria').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
						$('#nameStatus').html('(success)');
						$('#nameAria').show();
						if( res == true ) {
							checkFormStatus();
						} else {
							$('#submitBtn').attr('disabled', 'disabled'	);
							$('#addQuestion').attr('disabled', 'disabled');
						}
					}
				});
			}
		});
		
		$('#submitBtn').click(function(){
			$('#examForm').submit();
		});
		
		$('#addQuestion').click(function(){
			
			/* check if the exam_id already exists */
			/*examID = '<?= !empty( $exam->id ) ? $exam->id : 0 ?>';*/
			
			if( examID == 0 ){
				// add the exam in silent mode
				$.post('<?= site_url('ajax/save_exam') ?>', {action: 'save_new', name: $('#name').val(), description: $('#description').val(), passing_grade: $('#passing_grade').val(), time_allocation: $('#time_allocation').val()  } ).done(function( res ){
					examID = res;
					$('#examID').val(res);
					addQuestion();
					
				});
			} else {
				addQuestion();
			}
			
		});
		
	});
	
	function addQuestion()
	{
		questionCounter++;
		var randString = randomGenerator();
		
		/* var randomWord = 'divQuestion'+questionCounter; */
		var randomWord = 'divQuestion'+randString;
		
		var cntQuest = $('.divQuestion').length;
		if( cntQuest == 0 ){
			$('#divQuestion').append('<div class="box box-warning"><div class="box-header"><h3 class="box-title">QUESTIONS</h3></div><div class="questions"></div></div>');
		}
		
		$('.questions').append('<div id="'+randomWord+'" class="box-body divQuestion"></div>');
		var qHTML = '<div class="form-group fg" id="fg_'+randString+'">';
		qHTML += '<label for="question_'+randString+'" class="questionLabel" id="questionLabel_'+randString+'">Question #'+questionCounter+'</label>';
		qHTML += '<span class="delQuestion" id="delQuestion_'+randString+'" onclick="removeQuestion('+questionCounter+', 0)" style="float:right">x</span>';
		qHTML += '<textarea class="form-control questionInput textarea" id="question_'+randString+'" name="question_label[]"></textarea>'; 
		/*qHTML += '<input type="text" class="form-control questionInput" id="question_'+questionCounter+'" onkeyup="questionKeyup('+questionCounter+')" name="question_label[]" placeholder="Enter Question">';*/
		qHTML += '<div class="questionChoices" id="questionChoices_'+randString+'" style="display:none"></div>';
		qHTML += '<div id="btnAnswer_'+randString+'" class="btnAnswer col-md-12 text-right" style="padding-top:25px;"><button type="button" class="btn btn-info btn-sm addMultiple" onclick="addMultiple(\"'+randString+'\")"><i class="fa fa-server"></i>&nbsp;Add answer choice</button>&nbsp;<button type="button" class="btn btn-warning btn-sm addText" onclick="addText(\"'+randString+'\")"><i class="fa fa-file-text-o"></i>&nbsp;Add answer text</button></div>';
		qHTML += '</div>';
		$('#'+randomWord).append( qHTML );
		
		$.post( '<?= site_url('ajax/save_question') ?>', { exam_id: examID, name : '', position: questionCounter } ).done( function( question_id ) {
			
			$('#divQuestion'+randString).attr('id','divQuestion'+question_id);
			$('#fg_'+randString).attr('id','fg_'+question_id);
			$('#questionLabel_'+randString).attr('id','questionLabel_'+question_id);
			$('#questionLabel_'+question_id).attr('for','question_'+question_id);
			$('#delQuestion_'+randString).attr('id','delQuestion_'+question_id);
			$('#delQuestion_'+question_id).attr('onclick','removeQuestion('+questionCounter+','+question_id+')');
			$('#question_'+randString).attr('id','question_'+question_id);
			$('#questionChoices_'+randString).attr('id','questionChoices_'+question_id);
			
			$('#btnAnswer_'+randString).attr('id','btnAnswer_'+question_id);
			$('#btnAnswer_'+question_id+' .addMultiple').attr( 'onclick', 'addMultiple('+question_id+')' );
			$('#btnAnswer_'+question_id+' .addText').attr( 'onclick', 'addText('+question_id+')' );
			
			$('#fg_'+question_id).focus();
			tinyMCE.execCommand('mceAddEditor',false,'question_'+question_id);
		});
	}
	
	function loadEditor(id)
	{
		var instance = CKEDITOR.instances[id];
		if(instance) {
			CKEDITOR.remove(id);
		}
		CKEDITOR.replace(id);
	}
	
	function deleteOption(qid,sid, qType)
	{
		
		// delete on the database
		$.post( '<?= site_url('ajax/delete_choice') ?>', { id: sid } ).done(function(res){
			if( res == 'success' ){
				$('#qc_'+qid+'_'+sid).remove();
				var cntOptions = $('#questionChoices_'+qid+' .answerChoices').length;
				if( cntOptions > 0 ) {
					// reposition options
					i = 1;
					$('.optionText').each( function(){
						idVal = $(this).attr('id');
						idVal = idVal.replace('answers_', '');
						idVal = idVal.split('_');
						$.post( '<?= site_url('ajax/reposition_choice') ?>', { id: idVal[1], new_position : i } );
						i++;
					});
				} else {
					
					$('#btnAnswer_'+qid+' button').show();
					$('#btnAnswer_'+qid + ' .addMultiple').text('Add answer choice');
					$('#btnAnswer_'+qid + ' .addText').text('Add answer text');
					
				}
			}
		});
		
	}
	
	function addMultiple(id)
	{
		//hide the add text button
		var temp = randomGenerator();
		var totalChoices = $('#questionChoices_'+id+ ' .answerChoices').length;
		var choicesHtml = '<div class="answerChoices row" id="qc_'+id+'_'+ temp +'" style="padding-top:10px;"><label>'+ $('#fg_'+id+' .questionLabel').html() +' Choice '+ (totalChoices + 1) +':</label>';
		choicesHtml += '<span class="pull-right" onclick="deleteOption('+id+', "'+ temp +'", \'multiple\')">x</span>';
		choicesHtml += '<br>';
		choicesHtml += '<input id="choice_'+temp+'" type="radio" name="answers_correct_'+id+'" value="'+temp+'"> <label class="text-success"> Correct Answer?</label>';
		
		if( totalChoices == 0 ){
			$('#btnAnswer_'+id + ' .addMultiple').html('<i class="fa fa-server"></i>&nbsp;Add another answer choice');
			$('#btnAnswer_'+id + ' .addText').hide();
		}
		
		choicesHtml += '<div class="col-md-9 col-md-offset-3"><textarea class="textarea form-control optionText" name="answers_'+id+'[]" id="answers_'+ id +'_'+temp+'"></textarea></div>';
		choicesHtml += '</div>';
		
		$('#questionChoices_'+id).append( choicesHtml );
		$('#questionChoices_'+id).show();
		
		$.post('<?= site_url('ajax/save_choice') ?>', { question_id: id, position: totalChoices + 1, name: '' }).done( function(option_id){
			$('#qc_'+id+'_'+ temp).attr('id', 'qc_'+id+'_'+ option_id);
			$('#qc_'+id+'_'+ option_id+' .pull-right').attr('onclick', 'deleteOption('+id+','+option_id+', "multiple")');
			$('#choice_'+temp).attr('id','choice_'+option_id);
			$('#choice_'+option_id).val(option_id);
			$('#answers_'+ id +'_'+temp).attr('id','answers_'+ id +'_'+option_id);
			tinyMCE.execCommand('mceAddEditor',false,'answers_'+ id +'_'+option_id);	
		});
		
		
	}
	
	function addText(id)
	{
		// add the multiple button
		var temp = randomGenerator();
		var totalChoices = $('#questionChoices_'+id+ ' .answerChoices').length;
		var choicesHtml = '<div class="answerChoices row" id="qc_'+id+'_'+ temp +'" style="padding-top:10px;"><label>'+ $('#fg_'+id+' .questionLabel').html() +' Answer:</label>';
		choicesHtml += '<span class="pull-right" onclick="deleteOption('+id+', "'+ temp +'"), \'text\'">x</span>';
		
		if( totalChoices >= 0 ){
			$('#btnAnswer_'+id + ' .addText').html('<i class="fa fa-file-text-o"></i>&nbsp;Add another answer text');
			$('#btnAnswer_'+id + ' .addMultiple').hide();
		}
		
		choicesHtml += '<div class="col-md-9 col-md-offset-3"><input type="text" class="form-control optionText" name="answers_'+id+'[]" id="answers_'+ id +'_'+temp+'" value=""></div>';
		choicesHtml += '</div>';
		
		$('#questionChoices_'+id).append( choicesHtml );
		$('#questionChoices_'+id).show();
		
		$.post('<?= site_url('ajax/save_choice') ?>', { question_id: id, position: totalChoices + 1, type: 1, is_correct: 1, name: '' }).done( function(option_id){
			$('#qc_'+id+'_'+ temp).attr('id', 'qc_'+id+'_'+ option_id);
			$('#qc_'+id+'_'+ option_id+' .pull-right').attr('onclick', 'deleteOption('+id+','+option_id+', "text")');
			$('#answers_'+ id +'_'+temp).attr('id','answers_'+ id +'_'+option_id);
		});
	}
	
	function removeQuestion(id, questionID){
		
		// remove the question in the database
		$.post('<?= site_url('ajax/delete_question') ?>', { exam_id : examID, question_id: questionID, position: id });
		$('#divQuestion'+questionID).remove();
		questionCounter--;
		
		// update the naming of the labels
		var i = 1;
		$('.questionLabel').each(function(){
			qid = $(this).attr('id');
			qid = qid.replace('questionLabel_', '');
			$(this).html( 'Question #'+i );
			// reposition
			$.post("<?= site_url('ajax/reposition_question') ?>", { question_id: qid, new_position: i });
			i++;
		});
	}
	
	function randomGenerator() {
		var i = 0;
		var word = "";
		var vowels = new Array("a","e","u","i","o");
		var consonants = new Array("q","w","r","t","p","s","d","f","g","h","j","k","l","z","x","c","v","b","n","m");
		while(i < (16)){
			i++;
			word += vowels[Math.floor(Math.random() * vowels.length)] + consonants[Math.floor(Math.random() * consonants.length)];
		}
		return word;
	}
</script>