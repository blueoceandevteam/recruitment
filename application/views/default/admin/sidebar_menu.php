<ul class="sidebar-menu">
    <li>
        <a href="<?= site_url('admin/applicants')?>">
            <i class="fa fa-users"></i> <span>Applicants</span>
        </a>
    </li>
    <li>
        <a href="<?= site_url('admin/categories')?>">
            <i class="fa fa-list"></i> <span>Job Titles</span>
        </a>
    </li>
    <li>
        <a href="<?= site_url('admin/exams')?>">
            <i class="fa fa-th-large"></i> <span>Exams</span>
        </a>
    </li>
    <li>
        <a href="<?= site_url('admin/admingroups')?>">
            <i class="fa fa-gears"></i> <span>Roles</span>
        </a>
    </li>
    <li>
        <a href="<?= site_url('admin/admins')?>">
            <i class="fa fa-gear"></i> <span>Users</span>
        </a>
    </li>
    <li>
        <a href="<?= site_url('admin/roles')?>">
            <i class="fa fa-magic"></i> <span>Role Settings</span>
        </a>
    </li>
</ul>