<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?= $title; ?></title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href="<?= base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/ionicons.min.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/AdminLTE.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/styles.css'); ?>" rel="stylesheet" />
    <?= !empty($css) ? $css : ''; ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?= base_url('assets/js/html5shiv.min.js'); ?>"></script>
      <script src="<?= base_url('assets/js/respond.min.js'); ?>"></script>
    <![endif]-->
  </head>
  <body class="skin-blue">
    <?= $content; ?>
    <script src="<?= base_url('assets/js/jquery-1.11.2.min.js'); ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('assets/js/app.js'); ?>"></script>
    <?= !empty($js) ? $js : ''; ?>
  </body>
</html>