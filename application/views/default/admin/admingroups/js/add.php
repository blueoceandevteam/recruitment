<script type="text/javascript">
	$(document).ready(function(){
		
		$('input[type="checkbox"]').change(function(){
			var myName = $('#name').val();
			myName = $.trim(myName);
			if( myName != '' ) {
				$('#submitBtn').removeAttr('disabled');
			}
		});
		
		$('input[type="text"]').keypress(function(event){
			if( event.which == 13 ){
				if( ! $('#submitBtn').is(':disabled') ) $('#submitBtn').click();
			}
		});
		
		$('#description').keyup(function(){
			var myName = $('#name').val();
			myName = $.trim(myName);
			if( myName != '' ) {
				$('#submitBtn').removeAttr('disabled');
			}
		});
		
		$('#name').keyup(function(){
			$('#errorMessage').hide();
			var res = true;
			var tmp;
			tmp = $.trim( $(this).val() ) ;
			$(this).parent().attr('class', 'form-group');
			
			$('#nameAria').hide();
			if( tmp == '' ){
				$('#submitBtn').attr('disabled', 'disabled'	);
				$(this).parent().attr('class', 'form-group has-feedback has-error');
				$('#nameAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
				$('#nameStatus').html('(error)');
				$('#nameAria').show();
			} else {
				// check database
				$.post('<?= site_url('ajax/check_admingroup_name'); ?>', {name : tmp, id: $('#adminGroupID').val() }).done(function( result ){
					if( result == 'success' ){
						$('#field').html('name');
						$('#errorMessage').show();
						$('#submitBtn').attr('disabled', 'disabled'	);
						$('#name').parent().attr('class', 'form-group has-feedback has-error');
						$('#nameAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
						$('#nameStatus').html('(error)');
						$('#nameAria').show();
					} else {
						
						$('#name').parent().attr('class', 'form-group has-feedback has-success');
						$('#nameAria').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
						$('#nameStatus').html('(success)');
						$('#nameAria').show();
						if( res == true ) {
							$('#submitBtn').removeAttr('disabled');
						} else {
							$('#submitBtn').attr('disabled', 'disabled'	);
						}
					}
				});
			}
		});
		
		
		$('#submitBtn').click(function(){
			$('#adminGroupForm').submit();
		});
		
	});
</script>