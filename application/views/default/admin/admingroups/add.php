<div class="col-md-6 col-md-offset-3 ">
	<div class="box box-primary">
		<div class="box-body table-responsive">
			<form role="form" method="post" id="adminGroupForm">
				<div class="box-header">
					<div class="alert alert-danger" style="display: none" id="errorMessage">Role <span id="field"></span> already exists!</div>
				</div>
				<div class="box-body">
					<div class="form-group">
						<label for="name">Name</label>
						<input type="text" class="form-control" name="name" id="name" placeholder="Role" required autofocus aria-describedby="nameStatus" value="<?= !empty( $admingroup->name ) ? $admingroup->name : '' ?>" />
						<span id="nameAria" aria-hidden="true" style="display: none"></span>
						<span id="nameStatus" class="sr-only"></span>
					</div>
				</div>
				
				<div class="box-body">
					<div class="form-group">
						<label for="description">Description</label>
						<textarea id="description" class="form-control" name="description"><?= !empty( $admingroup->description ) ? $admingroup->description : '' ?></textarea>
					</div>
				</div>
				
				<div class="box-body">
					<div class="form-group">
						<label for="role_id">Admin Roles:</label>
						<br />
						
						<?php
						if( !empty( $roles ) ){
							foreach( $roles as $role ){
								$checked = '';
								if( !empty( $my_roles ) ){
									if( in_array( $role->id , $my_roles) ){
										$checked = ' checked';
									}
								}
								?>
						<input type="checkbox"<?= $checked?> value="<?= $role->id ?>" name="role_id[]" />&nbsp;<b class="text-primary" style="padding-left:10px"><?= $role->code ?></b>&nbsp;<small class="text-muted"><?= $role->description ?></small><br />
								<?php
							}
						}
						?>
					</div>
				</div>
				
				<div class="box-footer">
                    <button type="button" class="btn" onclick="window.location.href='<?= site_url('admin/admingroups') ?>'">Cancel</button>&nbsp;
                    <button type="button" class="btn btn-primary" disabled="disabled" id="submitBtn"><?= !empty( $admingroup->id ) ? 'Edit' : 'Add' ?> Role</button>
                </div>
                <input type="hidden" value="save_new" name="action" />
                <input type="hidden" value="<?= !empty( $admingroup->id ) ? $admingroup->id : '' ?>" name="id" id="adminGroupID" />
			</form>
		</div>
	</div>
</div>
