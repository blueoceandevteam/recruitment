<div class="col-md-6 col-md-offset-3 ">
	<div class="box box-primary">
		<div class="box-body table-responsive">
			<form role="form" method="post" id="adminForm">
				<div class="box-header">
					<div class="alert alert-danger" style="display: none" id="errorMessage">Admin <span id="field"></span> already exists!</div>
					<div class="alert alert-danger" style="display: none" id="errorPassword">Password and Confirm Password were different</div>
					<?php $flash_added = $this->session->userdata('edit_profile');
					$temp_id = !empty( $flash_added ) ? $flash_added : NULL;
					$this->session->set_userdata('edit_admin', NULL);
					if( !empty( $flash_added ) ){
						?>
					<div class="pull-left alert alert-success" style="margin: 10px" id="alertAction">Profile successfully modified!</div>
						<?php
					} ?>
				</div>
				<div class="box-body">
					<div class="form-group<?= !empty( $admin->id ) ? ' has-feedback has-success':'' ?>">
						<label for="username">Username</label>
						<input type="text" class="form-control" name="username" id="username" placeholder="Userame" required aria-describedby="usernameStatus" value="<?= !empty( $admin->username ) ? $admin->username : '' ?>" />
						<span id="usernameAria" aria-hidden="true"<?= !empty($admin->username) ? ' class="glyphicon glyphicon-ok form-control-feedback"' : ' style="display: none"' ?>></span>
						<span id="usernameStatus" class="sr-only"><?= !empty($admin->username) ? '(success)' : '' ?></span>
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" class="form-control" name="password" id="password" placeholder="Password" required aria-describedby="passwordStatus" value="" />
						<span id="passwordAria" aria-hidden="true" style="display: none"></span>
						<span id="passwordStatus" class="sr-only"></span>
					</div>
					<div class="form-group">
						<label for="confirm_password">Confirm Password</label>
						<input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Password" required aria-describedby="confirm_passwordStatus" value="" />
						<span id="confirm_passwordAria" aria-hidden="true" style="display: none"></span>
						<span id="confirm_passwordStatus" class="sr-only"></span>
					</div>
					<div class="form-group<?= !empty( $admin->id ) ? ' has-feedback has-success':'' ?>">
						<label for="first_name">First Name</label>
						<input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name" required aria-describedby="firstNameStatus" value="<?= !empty( $admin->first_name ) ? $admin->first_name : '' ?>" />
						<span id="firstNameAria" aria-hidden="true"<?= !empty($admin->cat_id) ? ' class="glyphicon glyphicon-ok form-control-feedback"' : ' style="display: none"' ?>></span>
						<span id="firstNameStatus" class="sr-only"><?= !empty($admin->cat_id) ? '(success)' : '' ?></span>
					</div>
					<div class="form-group<?= !empty( $admin->id ) ? ' has-feedback has-success':'' ?>">
						<label for="last_name">Last Name</label>
						<input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name" required aria-describedby="lastNameStatus" value="<?= !empty( $admin->last_name ) ? $admin->last_name : '' ?>" />
						<span id="lastNameAria" aria-hidden="true"<?= !empty($admin->cat_id) ? ' class="glyphicon glyphicon-ok form-control-feedback"' : ' style="display: none"' ?>></span>
						<span id="lastNameStatus" class="sr-only"><?= !empty($admin->cat_id) ? '(success)' : '' ?></span>
					</div>
				</div>
				
				<div class="box-footer">
                    <button type="button" class="btn" onclick="window.location.href='<?= site_url('admin/admins') ?>'">Cancel</button>&nbsp;
                    <button type="button" class="btn btn-primary"<?= !empty( $admin->id ) ? '' : ' disabled' ?> id="submitBtn"><?= !empty( $admin->id ) ? 'Edit' : 'Add' ?> Admin</button>
                </div>
                
                <input type="hidden" value="save_my_profile" name="action" />
			</form>
		</div>
	</div>
</div>