<div class="col-md-6 col-md-offset-3 ">
	<div class="box box-primary">
		<div class="box-body table-responsive">
			<form role="form" method="post" id="adminForm">
				<div class="box-header">
					<div class="alert alert-danger" style="display: none" id="errorMessage">Admin <span id="field"></span> already exists!</div>
				</div>
				<div class="box-body">
					<div class="form-group<?= !empty( $admin->id ) ? ' has-feedback has-success':'' ?>">
						<label for="title">Admin Group</label>
						<select class="form-control" name="group_id" id="group_id" required autofocus aria-describedby="group_idStatus">
							<option value="0" disabled<?= empty($admin->group_id) ? ' selected': '' ?>>Select Admin Group</option><?php
							if( !empty( $admingroups ) ) {
								foreach( $admingroups as $group ) { ?>
							<option value="<?= $group->id ?>"<?= !empty($admin->group_id) ? ($admin->group_id == $group->id ? ' selected' : '') : '' ?>><?= $group->name ?></option><?php
								}
							} ?>
						</select>
						<span id="group_idAria" aria-hidden="true"<?= !empty($admin->group_id) ? ' class="glyphicon glyphicon-ok form-control-feedback"' : '' ?> style="<?= empty($admin->group_id) ? 'display: none;' : '' ?>padding-right: 40px"></span>
						<span id="group_idStatus" class="sr-only"><?= !empty($admin->group_id) ? '(success)' : '' ?></span>
					</div>
					<div class="form-group<?= !empty( $admin->id ) ? ' has-feedback has-success':'' ?>">
						<label for="username">Username</label>
						<input type="text" class="form-control" name="username" id="username" placeholder="Userame" required aria-describedby="usernameStatus" value="<?= !empty( $admin->username ) ? $admin->username : '' ?>" />
						<span id="usernameAria" aria-hidden="true"<?= !empty($admin->username) ? ' class="glyphicon glyphicon-ok form-control-feedback"' : ' style="display: none"' ?>></span>
						<span id="usernameStatus" class="sr-only"><?= !empty($admin->username) ? '(success)' : '' ?></span>
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<input type="text" class="form-control" name="password" id="password" placeholder="Password" required aria-describedby="passwordStatus" value="" />
					</div>
					<div class="form-group<?= !empty( $admin->id ) ? ' has-feedback has-success':'' ?>">
						<label for="first_name">First Name</label>
						<input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name" required aria-describedby="firstNameStatus" value="<?= !empty( $admin->first_name ) ? $admin->first_name : '' ?>" />
						<span id="firstNameAria" aria-hidden="true"<?= !empty($admin->cat_id) ? ' class="glyphicon glyphicon-ok form-control-feedback"' : ' style="display: none"' ?>></span>
						<span id="firstNameStatus" class="sr-only"><?= !empty($admin->cat_id) ? '(success)' : '' ?></span>
					</div>
					<div class="form-group<?= !empty( $admin->id ) ? ' has-feedback has-success':'' ?>">
						<label for="last_name">Last Name</label>
						<input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name" required aria-describedby="lastNameStatus" value="<?= !empty( $admin->last_name ) ? $admin->last_name : '' ?>" />
						<span id="lastNameAria" aria-hidden="true"<?= !empty($admin->cat_id) ? ' class="glyphicon glyphicon-ok form-control-feedback"' : ' style="display: none"' ?>></span>
						<span id="lastNameStatus" class="sr-only"><?= !empty($admin->cat_id) ? '(success)' : '' ?></span>
					</div>
				</div>
				
				<div class="box-footer">
                    <button type="button" class="btn" onclick="window.location.href='<?= site_url('admin/admins') ?>'">Cancel</button>&nbsp;
                    <button type="button" class="btn btn-primary"<?= !empty( $admin->id ) ? '' : ' disabled' ?> id="submitBtn"><?= !empty( $admin->id ) ? 'Edit' : 'Add' ?> Admin</button>
                </div>
                
                <input type="hidden" value="save_new" name="action" />
                <input type="hidden" value="<?= !empty( $admin->id ) ? $admin->id : '' ?>" name="id" id="adminID" />
			</form>
		</div>
	</div>
</div>