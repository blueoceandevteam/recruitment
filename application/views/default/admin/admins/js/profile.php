<script type="text/javascript">
	
	function checkFormStatus(){
		
		$('#submitBtn').attr('disabled', 'disabled');
		
		// get the category value
		var firstNameVal = $('#first_name').val();
		firstNameVal = $.trim( firstNameVal );
		var lastNameVal = $('#last_name').val();
		lastNameVal = $.trim( lastNameVal );
		
		var passwordVal = $('#password').val();
		passwordVal = $.trim( passwordVal );
		var confirmPasswordVal = $('#confirm_password').val();
		confirmPasswordVal = $.trim( confirmPasswordVal );
		
		var res = true;
				
		if( firstNameVal == '' ){
			res = false;			
		}
		
		if( lastNameVal == '' ){
			res = false;
		}
		
		if( passwordVal != '' || confirmPasswordVal != '' ){
			if( passwordVal != confirmPasswordVal ){
				res = false;
			}
		}
		
		if( res ){
			$('#submitBtn').removeAttr('disabled');
		}
	}

	$(document).ready(function(){
		
		$('#alertAction').fadeOut(10000, function(){
	    	$(this).remove();
	    });
		
		$('input[type="text"]').keypress(function(event){
			if( event.which == 13 ){
				if( ! $('#submitBtn').is(':disabled') ) $('#submitBtn').click();
			}
		});
		
		$( '#first_name' ).keyup(function() {
			
			var firstNameVal = $(this).val();
			firstNameVal = $.trim( firstNameVal );
			
			if( firstNameVal == '' ){
				
				res = false;
				$('#first_name').parent().attr('class', 'form-group has-feedback has-error');
				$('#firstNameAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
				$('#firstNameStatus').html('(error)');
				$('#firstNameAria').show();
				
			} else {
				
				$('#first_name').parent().attr('class', 'form-group has-feedback has-success');
				$('#firstNameAria').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
				$('#firstNameStatus').html('(success)');
				$('#firstNameAria').show();
				
			}
			checkFormStatus();
		});
		
		$('#last_name').keyup(function(){
			
			var lastNameVal = $(this).val();
			lastNameVal = $.trim( lastNameVal );
			
			if( lastNameVal == '' ){
				res = false;
				
				$('#last_name').parent().attr('class', 'form-group has-feedback has-error');
				$('#lastNameAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
				$('#lastNameStatus').html('(error)');
				$('#lastNameAria').show();
			} else {
				
				$('#last_name').parent().attr('class', 'form-group has-feedback has-success');
				$('#lastNameAria').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
				$('#lastNameStatus').html('(success)');
				$('#lastNameAria').show();
			}
			
			checkFormStatus();
				
		});
		
		$('#username').keyup(function(){
			
			$('#errorMessage').hide();
			var res = true;
			var tmp;
			tmp = $.trim( $(this).val() ) ;
			$(this).parent().attr('class', 'form-group');
			$('#usernameAria').hide();
			if( tmp == '' ){
				$('#submitBtn').attr('disabled', 'disabled'	);
				$(this).parent().attr('class', 'form-group has-feedback has-error');
				$('#usernameAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
				$('#usernameStatus').html('(error)');
				$('#usernameAria').show();
			} else {
				// check database
				$.post('<?= site_url('ajax/check_admin_username'); ?>', {username : tmp, id: $('#adminID').val() }).done(function( result ){
					if( result == 'success' ){
						
						$('#field').html('username');
						$('#errorMessage').show();
						$('#submitBtn').attr('disabled', 'disabled'	);
						$('#username').parent().attr('class', 'form-group has-feedback has-error');
						$(this).parent().attr('class', 'form-group has-feedback has-error');
						$('#usernameAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
						$('#usernameStatus').html('(error)');
						$('#usernameAria').show();
						
					} else {
						
						$('#username').parent().attr('class', 'form-group has-feedback has-success');
						$(this).parent().attr('class', 'form-group has-feedback has-success');
						$('#usernameAria').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
						$('#usernameStatus').html('(success)');
						$('#usernameAria').show();
						
						if( res == true ) {
							checkFormStatus();
						} else {
							$('#submitBtn').attr('disabled', 'disabled'	);
						}
						
					}
				});
			}
		});
		
		$('#password').keyup(function(){
			
			$('#errorPassword').hide();
			var res = true;
			var tmp = $.trim( $(this).val() ) ;
			var cPassword = $.trim( $('#confirm_password').val() ) ;
			
			$(this).parent().attr('class', 'form-group');
			$('#passwordAria').hide();
			$('#confirm_passwordAria').hide();
			
			
			
			if( tmp == '' && cPassword == '' ){
				$(this).parent().attr('class', 'form-group');
				$('#passwordAria').attr('class', '');
				$('#passwordStatus').html('');
				$('#passwordAria').hide();
				
				$('#confirm_password').parent().attr('class', 'form-group');
				$('#confirm_passwordAria').attr('class', '');
				$('#confirm_passwordStatus').html('');
				$('#confirm_passwordAria').hide();
				
			} else {
				
				if( tmp == cPassword ){
					
					$(this).parent().attr('class', 'form-group has-feedback has-success');
					$('#passwordAria').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
					$('#passwordStatus').html('(success)');
					$('#passwordAria').show();
					
					$('#confirm_password').parent().attr('class', 'form-group has-feedback has-success');
					$('#confirm_passwordAria').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
					$('#confirm_passwordStatus').html('(success)');
					$('#confirm_passwordAria').show();
					
					if( res == true ) {
						checkFormStatus();
					} else {
						$('#submitBtn').attr('disabled', 'disabled'	);
					}
				} else {
					
					$('#errorPassword').show();
					$('#submitBtn').attr('disabled', 'disabled'	);
					
					$(this).parent().attr('class', 'form-group has-feedback has-error');
					$('#passwordAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
					$('#passwordStatus').html('(error)');
					$('#passwordAria').show();
					
					$('#confirm_password').parent().attr('class', 'form-group has-feedback has-error');
					$('#confirm_passwordAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
					$('#confirm_passwordStatus').html('(error)');
					$('#confirm_passwordAria').show();
				}
			}
		});
		
		$('#confirm_password').keyup(function(){
			
			$('#errorPassword').hide();
			var res = true;
			var tmp = $.trim( $('#password').val() )
			var cPassword = $.trim( $(this).val() ) ;
			
			$(this).parent().attr('class', 'form-group');
			$('#passwordAria').hide();
			$('#confirm_passwordAria').hide();
			
			if( tmp == '' && cPassword == '' ){
				$('#password').parent().attr('class', 'form-group');
				$('#passwordAria').attr('class', '');
				$('#passwordStatus').html('');
				$('#passwordAria').hide();
				
				$(this).parent().attr('class', 'form-group');
				$('#confirm_passwordAria').attr('class', '');
				$('#confirm_passwordStatus').html('');
				$('#confirm_passwordAria').hide();
				
			} else {
				
				if( tmp == cPassword ){
					
					
					$('#password').parent().attr('class', 'form-group has-feedback has-success');
					$('#passwordAria').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
					$('#passwordStatus').html('(success)');
					$('#passwordAria').show();
					
					$(this).parent().attr('class', 'form-group has-feedback has-success');
					$('#confirm_passwordAria').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
					$('#confirm_passwordStatus').html('(success)');
					$('#confirm_passwordAria').show();
					
					if( res == true ) {
						checkFormStatus();
					} else {
						$('#submitBtn').attr('disabled', 'disabled'	);
					}
				} else {
					
					$('#errorPassword').show();
					$('#submitBtn').attr('disabled', 'disabled'	);
					
					$('#password').parent().attr('class', 'form-group has-feedback has-error');
					$('#passwordAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
					$('#passwordStatus').html('(error)');
					$('#passwordAria').show();
					
					
					$(this).parent().attr('class', 'form-group has-feedback has-error');
					$('#confirm_passwordAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
					$('#confirm_passwordStatus').html('(error)');
					$('#confirm_passwordAria').show();
				}
			}
		});
		
		$('#submitBtn').click(function(){
			$('#adminForm').submit();
		});
		
	});
</script>