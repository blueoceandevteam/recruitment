<script src="<?= base_url('assets/js/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript">
	$(document).ready(function() {
	    $('#datatable').DataTable();
	    
	    $('#addCategory').click(function(){
	    	$('#hiddenForm input[name="action"]').val('add');
	    	$('#hiddenForm').submit();
	    });
	    
	    $('#alertAction').fadeOut(10000, function(){
	    	$(this).remove();
	    });
	    
	    setTimeout(function(){
	    	$('tr.success').removeAttr('class');
	    }, 10000);
	} );
	
	function deleteCategory(id)
	{
		var response = confirm("Are you sure you want to delete this job title?");
		if( response == true ){
			$('#hiddenForm input[name="action"]').val('delete');
			$('#hiddenForm input[name="id"]').val(id);
			$('#hiddenForm').submit();
		}
	}
	
	function editCategory(id)
	{
		$('#hiddenForm input[name="action"]').val('edit');
		$('#hiddenForm input[name="id"]').val(id);
		$('#hiddenForm').submit();
	}
</script>
