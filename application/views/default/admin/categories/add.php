<div class="col-md-6 col-md-offset-3 ">
	<div class="box box-primary">
		<div class="box-body table-responsive">
			<form role="form" method="post" id="categoryForm">
				<div class="box-header">
					<div class="alert alert-danger" style="display: none" id="errorMessage">Job Title <span id="field"></span> already exists!</div>
				</div>
				<div class="box-body">
					<div class="form-group">
						<label for="title">Title</label>
						<input type="text" class="form-control" name="title" id="title" placeholder="Title" required autofocus aria-describedby="titleStatus" value="<?= !empty( $category->title ) ? $category->title : '' ?>" />
						<span id="titleAria" aria-hidden="true" style="display: none"></span>
						<span id="titleStatus" class="sr-only"></span>
					</div>
					<div class="form-group">
						<label for="code">Code</label>
						<input type="text" class="form-control" name="code" id="code" placeholder="CODE" maxlength="10" required aria-describedby="codeStatus" value="<?= !empty( $category->code ) ? $category->code : '' ?>" />
						<span id="codeAria" aria-hidden="true" style="display: none"></span>
						<span id="codeStatus" class="sr-only"></span>
					</div>
				</div>
				<div class="box-footer">
                    <button type="button" class="btn" onclick="window.location.href='<?= site_url('admin/categories') ?>'">Cancel</button>&nbsp;
                    <button type="button" class="btn btn-primary" disabled="disabled" id="submitBtn"><?= !empty( $category->id ) ? 'Edit' : 'Add' ?> Job Title</button>
                </div>
                <input type="hidden" value="save_new" name="action" />
                <input type="hidden" value="<?= !empty( $category->id ) ? $category->id : '' ?>" name="id" id="categoryID" />
			</form>
		</div>
	</div>
</div>
