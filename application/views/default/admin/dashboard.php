<?= $header; ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <?= $sidebar_panel; ?>
                    <?= $sidebar_form; ?>
                    <?= $sidebar_menu; ?>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?= !empty( $page_title ) ? $page_title : ''; ?>
                        <small><?= !empty( $page_subtitle ) ? $page_subtitle : ''; ?></small>
                    </h1>
                    <?= $breadcumb; ?>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <?= !empty( $content ) ? $content : '' ?>
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->