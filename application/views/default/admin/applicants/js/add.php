<script src="<?= base_url('assets/js/jasny-bootstrap.min.js') ?>"></script>
<script type="text/javascript">
	
	function checkFormStatus(){
		
		$('#submitBtn').attr('disabled', 'disabled');
		
		// get the category value
		var catVal = $('#cat_id option:selected').val();
		var firstNameVal = $('#first_name').val();
		firstNameVal = $.trim( firstNameVal );
		var lastNameVal = $('#last_name').val();
		lastNameVal = $.trim( lastNameVal );
		var res = true;

		if( firstNameVal == '' ){
			res = false;
		}

		if( catVal == '0' || catVal == '' ){
			res = false;
		}

		if( lastNameVal == '' ){
			res = false;
		}

		if( res ){
			$('#submitBtn').removeAttr('disabled');
		}
	}

	$(document).ready(function(){
		
		$("#phone_number").keypress(function(evt){
			var charCode = (evt.which) ? evt.which : event.keyCode;
			if (charCode != 46 && charCode != 45 && charCode > 31 && (charCode < 48 || charCode > 57))
				return false;
			return true;
		});
		
		$('input[type="text"]').keypress(function(event){
			if( event.which == 13 ){
				if( ! $('#submitBtn').is(':disabled') ) $('#submitBtn').click();
			}
		});
		
		$("#cat_id").bind('change keyup', function(e) {
			var myOption = $('#cat_id option:selected').val();
			$.post('<?= site_url('ajax/generate_applicant_code') ?>', { categoryID: myOption, code: '<?= !empty($applicant->code) ? $applicant->code : ""; ?>' }).done(function(res){
				// get the input text
				$('#code').val( res );
				
				if( myOption == 0 ){
					$('#cat_id').parent().attr('class', 'form-group has-feedback has-error');
					$('#cat_idAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
					$('#cat_idStatus').html('(error)');
					$('#cat_idAria').show();
					
					$('#code').parent().attr('class', 'form-group has-feedback has-error');
					$('#codeAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
					$('#codeStatus').html('(error)');
					$('#codeAria').show();
					
				} else {
					
					$('#cat_id').parent().attr('class', 'form-group has-feedback has-success');
					$('#cat_idAria').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
					$('#cat_idStatus').html('(success)');
					$('#cat_idAria').show();
					
					$('#code').parent().attr('class', 'form-group has-feedback has-success');
					$('#codeAria').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
					$('#codeStatus').html('(success)');
					$('#codeAria').show();
				}
				checkFormStatus();
			});
		});
		
		$( '#first_name' ).keyup(function() {
			
			$('#duplicateMessage').hide();
			
			var firstNameVal = $(this).val();
			firstNameVal = $.trim( firstNameVal );
			
			if( firstNameVal == '' ){
				res = false;
				$('#first_name').parent().attr('class', 'form-group has-feedback has-error');
				$('#firstNameAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
				$('#firstNameStatus').html('(error)');
				$('#firstNameAria').show();
				
			} else {
				
				// check if there are any same names that already exists
				var lastNameVal = $('#last_name').val();
				lastNameVal = $.trim( lastNameVal );
				
				if( lastNameVal != '' ){
					$.post( "<?= site_url( 'ajax/check_names' )?>", { first_name: firstNameVal, last_name: lastNameVal } ).done(function( res ){
						if( res == 'true' ){
							// warns the admin that this user already exist
							$('#first_name').parent().attr('class', 'form-group has-feedback has-warning');
							$('#firstNameAria').attr('class', 'glyphicon glyphicon-exclamation-sign form-control-feedback');
							$('#firstNameStatus').html('(warning)');
							$('#firstNameAria').show();
							
							$('#last_name').parent().attr('class', 'form-group has-feedback has-warning');
							$('#lastNameAria').attr('class', 'glyphicon glyphicon-exclamation-sign form-control-feedback');
							$('#lastNameStatus').html('(warning)');
							$('#lastNameAria').show();
							
							$('#duplicateMessage').show();
						} else {
							$('#first_name').parent().attr('class', 'form-group has-feedback has-success');
							$('#firstNameAria').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
							$('#firstNameStatus').html('(success)');
							$('#firstNameAria').show();
							
							$('#last_name').parent().attr('class', 'form-group has-feedback has-success');
							$('#lastNameAria').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
							$('#lastNameStatus').html('(success)');
							$('#lastNameAria').show();
						}					
					});
				}  else {
					$('#first_name').parent().attr('class', 'form-group has-feedback has-success');
					$('#firstNameAria').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
					$('#firstNameStatus').html('(success)');
					$('#firstNameAria').show();
				}	
				
			}
			checkFormStatus();
		});
		
		$('#last_name').keyup(function(){
			
			$('#duplicateMessage').hide();
			
			var lastNameVal = $(this).val();
			lastNameVal = $.trim( lastNameVal );
			
			if( lastNameVal == '' ){
				res = false;
				
				$('#last_name').parent().attr('class', 'form-group has-feedback has-error');
				$('#lastNameAria').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
				$('#lastNameStatus').html('(error)');
				$('#lastNameAria').show();
			} else {
				
				// check if there are any same names that already exists
				var firstNameVal = $('#first_name').val();
				firstNameVal = $.trim( firstNameVal );
				
				if( firstNameVal != '' ){
					$.post( "<?= site_url( 'ajax/check_names' )?>", { first_name: firstNameVal, last_name: lastNameVal } ).done(function( res ){
						if( res == 'true' ){
							// warns the admin that this user already exist
							$('#first_name').parent().attr('class', 'form-group has-feedback has-warning');
							$('#firstNameAria').attr('class', 'glyphicon glyphicon-exclamation-sign form-control-feedback');
							$('#firstNameStatus').html('(warning)');
							$('#firstNameAria').show();
							
							$('#last_name').parent().attr('class', 'form-group has-feedback has-warning');
							$('#lastNameAria').attr('class', 'glyphicon glyphicon-exclamation-sign form-control-feedback');
							$('#lastNameStatus').html('(warning)');
							$('#lastNameAria').show();
							
							$('#duplicateMessage').show();
						} else {
							
							$('#last_name').parent().attr('class', 'form-group has-feedback has-success');
							$('#lastNameAria').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
							$('#lastNameStatus').html('(success)');
							$('#lastNameAria').show();
							
							$('#first_name').parent().attr('class', 'form-group has-feedback has-success');
							$('#firstNameAria').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
							$('#firstNameStatus').html('(success)');
							$('#firstNameAria').show();
							
						}					
					});
				} else {
					$('#last_name').parent().attr('class', 'form-group has-feedback has-success');
					$('#lastNameAria').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
					$('#lastNameStatus').html('(success)');
					$('#lastNameAria').show();
				}
			}
			
			checkFormStatus();
				
		});
		
		$('#submitBtn').click(function(){
			$('#applicantForm').submit();
		});
		
	});
</script>