<div class="box box-primary">
	<div class="box-header">
		<?php
		
		$temp_id = 0;
		
		$flash_added = $this->session->userdata('new_applicant');
		$temp_id = !empty( $flash_added ) ? $flash_added : $temp_id;
		$this->session->set_userdata('new_applicant', NULL);
		if( !empty( $flash_added ) ){
			?>
		<div class="pull-left alert alert-success" style="margin: 10px" id="alertAction">Applicant successfully added!</div>
			<?php
		}
		
		$flash_added = $this->session->userdata('edit_applicant');
		$temp_id = !empty( $flash_added ) ? $flash_added : $temp_id;
		$this->session->set_userdata('edit_applicant', NULL);
		if( !empty( $flash_added ) ){
			?>
		<div class="pull-left alert alert-success" style="margin: 10px" id="alertAction">Applicant successfully modified!</div>
			<?php
		}
		
		$flash_added = $this->session->userdata('delete');
		$temp_id = !empty( $flash_added ) ? $flash_added : $temp_id;
		$this->session->set_userdata('delete', NULL);
		if( !empty( $flash_added ) ){
			?>
		<div class="pull-left alert alert-danger" style="margin: 10px" id="alertAction">Applicant successfully deleted!</div>
			<?php
		}
		if( in_array( 'Add Applicant' , $my_roles) ){
		?>
		<button class="btn btn-success pull-right btn-lg" style="margin: 10px;" type="button" id="addApplicant">
			<i class="fa fa-plus-square-o"></i> Add New
		</button>
		<?php } ?>
	</div>
	<div class="box-body table-responsive">
		<?php if( !empty( $applicants ) ) {
			?>
		<table class="table" id="datatable" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Code</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Phone Number</th>
					<th>Actions</th>
				</tr>
			</thead>
			<?php
			if( count( $applicants ) > 20 ){
				?>
			<tfoot>
				<tr>
					<th>Code</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Phone Number</th>
					<th>Actions</th>
				</tr>
			</tfoot>
				<?php
			}
			?>
			
			<tbody>
			<?php
				foreach( $applicants as $applicant )
				{
					?>
			<tr<?= $applicant->id == $temp_id ? ' class="success"' : '' ?>>
				<td><?= $applicant->code ?></td>
				<td><?= $applicant->first_name ?></td>
				<td><?= $applicant->last_name ?></td>
				<td><?= $applicant->phone_number ?></td>
				<td>
					<?php if( in_array( 'Add Applicant' , $my_roles) ) { ?>
					<button class="btn btn-primary btn-sm" type="button" onclick="editApplicant(<?= $applicant->id ?>)">
						<i class="fa fa-pencil"></i> Edit
					</button>
					<?php }
					if( in_array( 'Add Applicant' , $my_roles) ) { ?>
					<button class="btn btn-danger btn-sm" type="button" onclick="deleteApplicant(<?= $applicant->id ?>)">
						<i class="fa fa-trash"></i> Delete
					</button>
					<?php } ?>
				</td>
			</tr>
					<?php
				}
			?>
			</tbody>
		</table>
			<?php
		} else {
			echo 'no records yet...';
		} ?>
		
	</div>
</div>
<form style="display: none" id="hiddenForm" method="POST">
	<input type="hidden" name="action" value="" />
	<input type="hidden" name="id" value="" />
</form>