<div class="col-md-6 col-md-offset-3 ">
	<div class="box box-primary">
		<div class="box-body table-responsive">
			<form role="form" method="post" id="applicantForm" enctype="multipart/form-data">
				<div class="box-header">
					<div class="alert alert-danger" style="display: none" id="errorMessage">Applicant <span id="field"></span> already exists!</div>
				</div>
				<div class="box-body">
					<div class="form-group<?= !empty( $applicant->id ) ? ' has-feedback has-success':'' ?>">
						<label for="title">Category</label>
						<select class="form-control" name="cat_id" id="cat_id" required autofocus aria-describedby="cat_idStatus" >
							<option value="0" disabled<?= empty($applicant->cat_id) ? ' selected': '' ?>>Select a category</option><?php
							if( !empty( $categories ) ) {
								foreach( $categories as $cat ) { ?>
							<option value="<?= $cat->id ?>"<?= !empty($applicant->cat_id) ? ($applicant->cat_id == $cat->id ? ' selected' : '') : '' ?>><?= $cat->title ?></option><?php
								}
							} ?>
						</select>
						<span id="cat_idAria" aria-hidden="true"<?= !empty($applicant->cat_id) ? ' class="glyphicon glyphicon-ok form-control-feedback"' : '' ?> style="<?= empty($applicant->cat_id) ? 'display: none;' : '' ?>padding-right: 40px"></span>
						<span id="cat_idStatus" class="sr-only"><?= !empty($applicant->cat_id) ? '(success)' : '' ?></span>
					</div>
					
					<div class="form-group<?= !empty( $applicant->id ) ? ' has-feedback has-success':'' ?>">
						<label for="code">Code</label>
						<input readonly type="text" class="form-control" name="code" id="code" placeholder="CODE" required aria-describedby="codeStatus" value="<?= !empty( $applicant->code ) ? $applicant->code : '' ?>" />
						<span id="codeAria" aria-hidden="true"<?= !empty($applicant->cat_id) ? ' class="glyphicon glyphicon-ok form-control-feedback"' : ' style="display: none"' ?>></span>
						<span id="codeStatus" class="sr-only"><?= !empty($applicant->cat_id) ? '(success)' : '' ?></span>
					</div>
					
					<div class="form-group has-feedback has-success">
						<label for="passcode">Pass Code</label>
						<input readonly type="text" class="form-control" name="passcode" id="passcode" placeholder="Pass Code" required aria-describedby="passcodeStatus" value="<?= !empty( $applicant->passcode ) ? $applicant->passcode : $passcode ?>" />
						<span id="passcodeAria" aria-hidden="true" class="glyphicon glyphicon-ok form-control-feedback"></span>
						<span id="passcodeStatus" class="sr-only">(success)</span>
					</div>
					
					<div class="form-group<?= !empty( $applicant->id ) ? ' has-feedback has-success':'' ?>">
						<label for="first_name">First Name</label>
						<input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name" required aria-describedby="firstNameStatus" value="<?= !empty( $applicant->first_name ) ? $applicant->first_name : '' ?>" />
						<span id="firstNameAria" aria-hidden="true"<?= !empty($applicant->cat_id) ? ' class="glyphicon glyphicon-ok form-control-feedback"' : ' style="display: none"' ?>></span>
						<span id="firstNameStatus" class="sr-only"><?= !empty($applicant->cat_id) ? '(success)' : '' ?></span>
					</div>
					
					<div class="form-group<?= !empty( $applicant->id ) ? ' has-feedback has-success':'' ?>">
						<label for="last_name">Last Name</label>
						<input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name" required aria-describedby="lastNameStatus" value="<?= !empty( $applicant->last_name ) ? $applicant->last_name : '' ?>" />
						<span id="lastNameAria" aria-hidden="true"<?= !empty($applicant->cat_id) ? ' class="glyphicon glyphicon-ok form-control-feedback"' : ' style="display: none"' ?>></span>
						<span id="lastNameStatus" class="sr-only"><?= !empty($applicant->cat_id) ? '(success)' : '' ?></span>
					</div>
					
					<div class="box-header">
						<div class="alert alert-warning" style="display: none" id="duplicateMessage"><b>WARNING!</b> Applicant <b>first name</b> and <b>last name</b> already exists! Kindly check if the applicant already exists.</div>
					</div>
					
					<div class="form-group>
						<label for="phone_number">Phone Number</label>
						<input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="Phone Number" value="<?= !empty( $applicant->phone_number ) ? $applicant->phone_number : '' ?>" />
					</div>
					
					<div class="form-group">
						<label for="phone_number">Resume</label>
						<div class="fileinput fileinput-new input-group" data-provides="fileinput">
							<div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
							<span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="resume"></span>
							<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
						</div> <?php
						if( !empty( $applicant->resume ) ){
							?>
							<a class="btn btn-success" href="<?= site_url( 'download/get/'.$applicant->resume ) ?>">
								<i class="fa fa-download fa-lg"></i> View Resume
							</a>
							<?php	
						} ?>
						
					</div>
					
				</div>
				
				<div class="box-footer">
                    <button type="button" class="btn" onclick="window.location.href='<?= site_url('admin/applicants') ?>'">Cancel</button>&nbsp;
                    <button type="button" class="btn btn-primary"<?= !empty( $applicant->id ) ? '' : ' disabled' ?> id="submitBtn"><?= !empty( $applicant->id ) ? 'Edit' : 'Add' ?> Applicant</button>
                </div>
                
                <input type="hidden" value="save_new" name="action" />
                <input type="hidden" value="<?= !empty( $applicant->id ) ? $applicant->id : '' ?>" name="id" id="applicantID" />
			</form>
		</div>
	</div>
</div>