<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Applicant extends CI_Controller {
	
	private $template = 'default/applicant/';
	private $title = 'Select Exams';
	private $content = '';
	private $css;
	private $data;
	private $header;
	private $js;
	
	public function __construct()
	{
		
		parent::__construct();
		
		$this->data['header'] = $this->load->view($this->template.'header', NULL, TRUE);
		$this->data['sidebar_panel'] = $this->load->view($this->template.'user_panel', NULL, TRUE);
		$this->data['sidebar_form']= $this->load->view($this->template.'sidebar_form', NULL, TRUE);
		$this->data['sidebar_menu'] = $this->load->view($this->template.'sidebar_menu', NULL, TRUE);
		$this->data['breadcumb'] = $this->load->view($this->template.'breadcumb', NULL, TRUE);
		$this->data['page_title'] = 'Exams';
		$this->data['page_subtitle'] = 'start now';

		$this->load->model('Applicants_model',      'appl_model');
		$this->load->model('Applicants_exam_model', 'exam_model');

	}
	
	public function index()
	{
		// check if user is logged in
		if( self::_check_logged() ){
			self::_exams();
		} else {
			self::_login();	
		}
		
		self::_finalize();
	}
	
	public function applicants()
	{
		if( self::_check_logged() ){
			$this->title = 'Applicants';
			self::_applicants();
		} else {
			self::_login();
		}
		
		self::_finalize();
	}

	#start exam
	public function start($id = false)
	{
		if( self::_check_logged() ){
			$this->title = 'Start Exam Now!';
			$this->js = $this->load->view($this->template.'js/exams.js.php', NULL, TRUE);
			self::_start_exam($id);
		} else {
			self::_login();
		}
		
		self::_finalize();
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect('applicant');
	}
	
	private function _start_exam($id = false, $info = false)
	{	
		$content_data = array();

		#exam info here. $id is the exam id
		#$exam_info = $this->_select_exam_info($id);

		$select_question = $this->_select_questions($id);

		if(is_object($select_question)):
		foreach($select_question->result_array() as $vdata):
			$content_data[] = array('question'=> $vdata,
									'choices' => $this->_select_questions_choices($vdata['id']));
			
		endforeach;
		endif;
		$data['questions'] = (!empty($content_data)) ? $content_data : '';
		$this->data['content'] = $this->load->view($this->template.'exams/exams-questions', $data , TRUE);
		$data = $this->data;
		$this->content = $this->load->view($this->template.'applicant-page', $data, TRUE);
	}

	private function _exams()
	{	
		$data['exam_list'] = $this->_select_exams();
		$this->data['content'] = $this->load->view($this->template.'exams/exams-list', $data , TRUE);
		$data = $this->data;
		$this->content = $this->load->view($this->template.'applicant-page', $data, TRUE);
	}
	
	private function _finalize()
	{
		$data['title'] = $this->title;
		$data['content'] = $this->content;
		$data['js'] = $this->js;
		$data['css'] = $this->css;
		$this->load->view($this->template.'template', $data);
	}
	
	private function _login()
	{
		$this->title = 'Sign in';
		$this->content = $this->load->view($this->template.'login', NULL, TRUE);
		$this->js = $this->load->view($this->template.'js/login', NULL, TRUE);
	}
	
	private function _check_logged()
	{
		return $this->session->userdata('logged_in_applicant');
	}

	
	/*Exam functionalities*/
	private function _select_exams()
	{
		$result = array();
		$ex_info = $this->exam_model->get_exam_info();
		
		foreach($ex_info as $kinfo => $vinfo):
			$result[$kinfo] = $vinfo;
		endforeach;
		
		return $result;
	}

	private function _select_questions($id = false)
	{
		$id = intval($id);
		if(empty($id))
			return false;
		
		$questions = $this->exam_model->get_exam_questions($id);

		return $questions;
	}

	private function _select_questions_choices($id)
	{
		$id = intval($id);
		if(empty($id))
			return false;
		
		$questions = $this->exam_model->get_exam_question_choices($id);

		return $questions;
	}

	/*private function _select_exam_info($id)
	{
		$id = intval($id);
		if(empty($id))
			return false;
		
		$information = $this->exam_model->get_exam_info($id);

		return $information;
	}*/
}
