<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {
	
	public function generate_applicant_code($digits = 5)
	{
		$result = '';
		$category_id = $this->input->post('categoryID');
		$code = $this->input->post('code');
		
		if( !empty( $category_id ) ){
			
			if( !isset( $this->m_Applicant ) ) $this->load->model( 'Applicants_model', 'm_Applicant' );
			
			if( !empty( $code ) ){
				$exist = $this->m_Applicant->check_applicant_code( $category_id, $code );
				if( $exist ){
					$result = $code;
				}
			}
			
			if( empty( $result ) ){
				$res = $this->m_Applicant->get_total_per_category( $category_id );
				$res++;
				
				// get the code of the category
				if( !isset( $this->m_Cat ) ) $this->load->model( 'Categories_model', 'm_Cat' );
				$gen_code = $this->m_Cat->get_category_code( $category_id );
				$result = $gen_code.str_pad( $res , $digits, '0', STR_PAD_LEFT);	
			}
		}
		
		echo $result;
	}
	
	public function check_category_title()
	{
		$result = 'failed';
		$title = $this->input->post('title');
		$title = trim($title);
		if( !empty( $title ) ){
			if( !isset( $this->m_Cat ) ) $this->load->model( 'Categories_model', 'm_Cat' );
			$res = $this->m_Cat->check_category_title($title);
			if( $res ) $result = 'success';
		}
		echo $result;
	}
	
	public function check_admingroup_name()
	{
		$result = 'failed';
		$name = $this->input->post('name');
		$name = trim($name);
		if( !empty( $name ) ){
			if( !isset( $this->mAdminGroup ) ) $this->load->model( 'Admingroups_model', 'mAdminGroup' );
			$res = $this->mAdminGroup->check_admingroup_name($name);
			if( $res ) $result = 'success';
		}
		echo $result;
	}
	
	public function check_exam_name()
	{
		$result = 'failed';
		$name = $this->input->post('name');
		$name = trim($name);
		if( !empty( $name ) ){
			if( !isset( $this->mExams ) ) $this->load->model( 'Modules_model', 'mExams' );
			$res = $this->mExams->check_exam_name($name);
			if( $res ) $result = 'success';
		}
		echo $result;
	}
	
	public function check_role_code()
	{
		$result = 'failed';
		$code = $this->input->post('code');
		$code = trim($code);
		if( !empty( $code ) ){
			if( !isset( $this->mRole ) ) $this->load->model( 'Roles_model', 'mRole' );
			$res = $this->mRole->check_role_code($code);
			if( $res ) $result = 'success';
		}
		echo $result;
	}
	
	public function check_category_code()
	{
		$result = 'failed';
		$code = $this->input->post('code');
		$code = trim($code);
		if( !empty( $code ) ){
			if( !isset( $this->m_Cat ) ) $this->load->model( 'Categories_model', 'm_Cat' );
			$res = $this->m_Cat->check_category_code($code);
			if( $res ) $result = 'success';
		}
		echo $result;
	}

	
	public function check_admin_username()
	{
		$result = 'failed';
		$username = $this->input->post('username');
		$username = trim($username);
		if( !empty( $username ) ){
			if( !isset( $this->mAdmins ) ) $this->load->model( 'Admins_model', 'mAdmins' );
			$res = $this->mAdmins->check_admin_username($username);
			if( $res ) $result = 'success';
		}
		echo $result;
	}

	public function check_module_code()
	{
		$result = 'failed';
		$code = $this->input->post('code');
		$code = trim($code);
		if( !empty( $code ) ){
			if( !isset( $this->mModules ) ) $this->load->model( 'Modules_model', 'mModules' );
			$res = $this->mModules->check_module_code($code);
			if( $res ) $result = 'success';
		}
		echo $result;
	}
	
	public function authenticate($id = false){
		
		if($id == 'applicant') return $this->_authenticate_applicant($id);
		
		$result = 'failed';
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		if( !empty( $username ) && !empty( $password ) ){
			if( !isset( $this->m_User ) ) $this->load->model( 'User_model', 'mUser' );
			$res = $this->mUser->check_account($username, $password);
			if( $res ) $result = 'success';
		}
		
		echo $result;
	}
	
	public function session_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		if( !empty( $username ) && !empty( $password ) ){
			if( !isset( $this->m_User ) ) $this->load->model( 'User_model', 'mUser' );
			$res = $this->mUser->check_account($username, $password);
			if( $res ){
				$user_id = $this->mUser->get_user_id($username, $password);
				$data = array( 'logged_in' => 1, 'username' => $this->input->post('username'), 'user_id' => $user_id );
				$this->session->set_userdata($data);
			}
		}
	}
	
	public function check_names(){
		
		$result = 'false';
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		
		$first_name = trim($first_name);
		$last_name = trim($last_name);
		
		if( !empty( $first_name ) && !empty( $last_name )) {
			if( !isset( $this->mApplicants ) ) $this->load->model('Applicants_model', 'mApplicants');
		
			$res = $this->mApplicants->check_first_last_names( $first_name, $last_name );
			
			if( $res ) $result = 'true';
			
		}
		
		echo $result;
	}
	
	public function save_question()
	{
		// exam_id
		if( !isset( $this->mQuestions ) ) $this->load->model('Questions_model', 'mQuestions');
		echo $this->mQuestions->save_question();
	}
	
	public function save_choice()
	{
		// exam_id
		if( !isset( $this->mChoices ) ) $this->load->model('Choices_model', 'mChoices');
		echo $this->mChoices->save_choice();
	}
	
	public function save_exam()
	{
		if( !isset( $this->mExams ) ) $this->load->model('Modules_model', 'mExams');
		echo $this->mExams->save_new();
	}
	
	public function delete_question()
	{
		if( !isset( $this->mQuestions ) ) $this->load->model('Questions_model', 'mQuestions');
		$result = $this->mQuestions->delete_question();
		echo $result ? 'success' : 'failed';
	}
	
	public function delete_choice()
	{
		if( !isset( $this->mChoices ) ) $this->load->model('Choices_model', 'mChoices');
		$result = $this->mChoices->delete_choice();
		echo $result ? 'success' : 'failed';
	}
	
	public function reposition_question()
	{
		if( !isset( $this->mQuestions ) ) $this->load->model('Questions_model', 'mQuestions');
		$result = $this->mQuestions->reposition_question();
		echo $result ? 'success' : 'failed';
	}
	
	public function reposition_choice()
	{
		if( !isset( $this->mQuestions ) ) $this->load->model('Questions_model', 'mQuestions');
		$result = $this->mQuestions->reposition_choice();
		echo $result ? 'success' : 'failed';
	}

	#Applicant Authenticity joseph
	private function _authenticate_applicant($id){
		
		$result   = 'failed';
		
		$temp = $this->input->post('username');
		$code     = !empty($temp) ? $temp : '';
		
		$temp = $this->input->post('password');
		$passcode = !empty($temp) ? $temp : '';

		if( !empty( $code) && !empty( $passcode ) ){
			if( !isset( $this->mApplicants ) ) $this->load->model('Applicants_model', 'mApplicants');
				$res = $this->mApplicants->check_applicant_auth($code, $passcode);
			
			if( $res ): 
				$result = 'success';
				$data = array( 'logged_in_applicant' => $res, 
							   'app_code'     => $code, 
							   'app_passcode' => $passcode );
				$this->session->set_userdata($data);
			endif;
		}
		echo $result;
	}

	public function saveanswer(){
		$result = 'failed';
		$this->load->model('Applicants_exam_model', 'exam_model');

		$question_id =  $this->input->post('quest_id');
		$answer      =  $this->input->post('answer');

		if(!empty($question_id) AND !empty($answer))
			$result = $this->exam_model->insert_user_answer($question_id, $answer);
		
		if(intval($result) > 0)
			$result = 'success';

		echo $result;
	}
	
	/*public function test()
	{
		if( !isset( $this->m_User ) ) $this->load->model( 'User_model', 'mUser' );
		echo $this->mUser->create_password('portal9901!');
	}*/
	
}