<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	private $template = 'default/admin/';
	private $title = 'Administration';
	private $content = '';
	private $css;
	private $data;
	private $header;
	private $js;
	private $my_roles = array();
	
	public function __construct()
	{
		
		parent::__construct();
		
		$this->data['header'] = $this->load->view($this->template.'header', NULL, TRUE);
		$this->data['sidebar_panel'] = $this->load->view($this->template.'user_panel', NULL, TRUE);
		$this->data['sidebar_form']= $this->load->view($this->template.'sidebar_form', NULL, TRUE);
		$this->data['sidebar_menu'] = $this->load->view($this->template.'sidebar_menu', NULL, TRUE);
		$this->data['breadcumb'] = $this->load->view($this->template.'breadcumb', NULL, TRUE);
		$this->data['page_title'] = 'Dashboard';
		$this->data['page_subtitle'] = 'List of Menus';
		
		// get user's access
		if( !isset( $this->mRoles ) ) $this->load->model('Roles_model', 'mRoles');
		$this->my_roles = $this->mRoles->get_current_user_roles();
		
	}
	
	public function index()
	{
		// check if user is logged in
		if( self::_check_logged() ){
			self::_dashboard();
		} else {
			self::_login();	
		}
		
		self::_finalize();
	}
	
	public function applicants()
	{
		if( self::_check_logged() ){
			$this->data['page_title'] = 'Applicants';
			self::_applicants();
		} else {
			self::_login();
		}
		
		self::_finalize();
	}
	
	public function categories()
	{
		if( self::_check_logged() ){
			$this->data['page_title'] = 'Job Titles';
			self::_categories();
		} else {
			self::_login();
		}
		
		self::_finalize();
	}
	
	public function roles()
	{
		if( self::_check_logged() ){
			$this->data['page_title'] = 'Admin Roles';
			self::_roles();
		} else {
			self::_login();
		}
		
		self::_finalize();
	}
	
	public function admingroups()
	{
		if( self::_check_logged() ){
			$this->data['page_title'] = 'Roles';
			self::_admingroups();
		} else {
			self::_login();
		}
		
		self::_finalize();
	}

	
	public function admins()
	{
		if( self::_check_logged() ){
			$this->data['page_title'] = 'Admins';
			self::_admins();
		} else {
			self::_login();
		}
		
		self::_finalize();
	}
	
	public function profile()
	{
		if( self::_check_logged() ){
			
			$this->data['page_title'] = 'Profile';
			$action = $this->input->post('action');
		
			$breadcumbs['items'] = array( 'admins' => 'Admins' );
			
			if( !isset( $this->mAdminGroups ) ) $this->load->model('Admingroups_model', 'mAdminGroups');
			if( !isset( $this->mAdmins ) ) $this->load->model('Admins_model', 'mAdmins');
			$this->mAdmins->save_my_profile();
			$this->data['page_subtitle'] = 'Edit Profile';
			$breadcumbs['items']['#'] = 'Edit Profile';
			
			$admin = NULL;
			
			$admin['admin'] = $this->mAdmins->admin_detail( $this->session->userdata('user_id')  );
			
			$this->data['content'] = $this->load->view( $this->template.'admins/profile', $admin, TRUE );
			$this->js = $this->load->view( $this->template.'admins/js/profile', $admin, TRUE );	
			
			$this->data['breadcumb'] = $this->load->view($this->template.'breadcumb', $breadcumbs, TRUE);
			$this->content = $this->load->view($this->template.'dashboard', $this->data, TRUE);
		} else {
			self::_login();
		}
		
		self::_finalize();
	}
	
	public function exams()
	{
		if( self::_check_logged() ){
			$this->data['page_title'] = 'Exams';
			self::_exams();
		} else {
			self::_login();
		}
		
		self::_finalize();
	}
	
	private function _applicants()
	{
		$action = $this->input->post('action');
		
		$breadcumbs['items'] = array( 'applicants' => 'Applicants' );
		
		if( !isset( $this->mCategories ) ) $this->load->model('Categories_model', 'mCategories');
		if( !isset( $this->mApplicants ) ) $this->load->model('Applicants_model', 'mApplicants');
		
		switch ( $action ) {
			case 'add':
			case 'edit':
				
				$this->data['page_subtitle'] = 'Add New Applicant';
				$breadcumbs['items']['#'] = 'Add New Applicant';
				
				$applicant = NULL;
				if( $action == 'edit' ){
					$this->data['page_subtitle'] = 'Edit Applicant';
					$breadcumbs['items']['#'] = 'Edit Applicant';
					$applicant['applicant'] = $this->mApplicants->applicant_detail( $this->input->post('id') );
				}
				
				// get the list of categories
				
				$applicant['categories'] = $this->mCategories->get_all();
				$applicant['passcode'] = $this->mCategories->generate_random_string();
				
				if( (!in_array( 'Add Applicant' , $this->my_roles) && $action == 'add') || (!in_array( 'Edit Applicant' , $this->my_roles) && $action == 'edit')){
					$this->data['content'] = 'Restricted Access.';
				} else {
					$this->data['content'] = $this->load->view( $this->template.'applicants/add', $applicant, TRUE );
				}
				
				$this->css = $this->load->view( $this->template.'applicants/css/add', $applicant, TRUE );	
				$this->js = $this->load->view( $this->template.'applicants/js/add', $applicant, TRUE );
				
				break;
				
			case 'read':
				break;
			// by default, it is to view all
			case 'delete':
				$this->mApplicants->delete_applicant();
			case 'view':
			case 'save_new':
				// add to database here
				$this->mApplicants->save_new();
			default:
				
				$this->data['page_subtitle'] = 'View Applicants';
				$breadcumbs['items']['#'] = 'View All';
				
				// get the list of all the categories
				
				$params['applicants'] = $this->mApplicants->get_all();
				$params['my_roles'] = $this->my_roles;
				
				$this->data['content'] = $this->load->view( $this->template.'applicants/view_all', $params, TRUE );
				$this->js = $this->load->view( $this->template.'applicants/js/view_all', NULL, TRUE );
				$this->css = $this->load->view( $this->template.'applicants/css/view_all', NULL, TRUE );
				break;
		}
		$this->data['breadcumb'] = $this->load->view($this->template.'breadcumb', $breadcumbs, TRUE);
		$this->content = $this->load->view($this->template.'dashboard', $this->data, TRUE);
	}

	private function _admins()
	{
		$action = $this->input->post('action');
		
		$breadcumbs['items'] = array( 'admins' => 'Admins' );
		
		if( !isset( $this->mAdminGroups ) ) $this->load->model('Admingroups_model', 'mAdminGroups');
		if( !isset( $this->mAdmins ) ) $this->load->model('Admins_model', 'mAdmins');
		
		switch ( $action ) {
			case 'add':
			case 'edit':
				
				$this->data['page_subtitle'] = 'Add New Admin';
				$breadcumbs['items']['#'] = 'Add New Admin';
				
				$admin = NULL;
				
				if( $action == 'edit' ){
					$this->data['page_subtitle'] = 'Edit Admin';
					$breadcumbs['items']['#'] = 'Edit Admin';
					$admin['admin'] = $this->mAdmins->admin_detail( $this->input->post('id') );
				}
				
				// get the list of categories
				$admin['admingroups'] = $this->mAdminGroups->get_all();
				
				$this->data['content'] = $this->load->view( $this->template.'admins/add', $admin, TRUE );
				$this->js = $this->load->view( $this->template.'admins/js/add', $admin, TRUE );	
				
				break;
				
			case 'read':
				break;
			// by default, it is to view all
			case 'delete':
				$this->mAdmins->delete_admin();
			case 'view':
			case 'save_new':
				// add to database here
				$this->mAdmins->save_new();
			default:
				
				$this->data['page_subtitle'] = 'View Admins';
				$breadcumbs['items']['#'] = 'View All';
				
				// get the list of all the categories
				
				$params['admins'] = $this->mAdmins->get_all();
				
				$this->data['content'] = $this->load->view( $this->template.'admins/view_all', $params, TRUE );
				$this->js = $this->load->view( $this->template.'admins/js/view_all', NULL, TRUE );
				$this->css = $this->load->view( $this->template.'admins/css/view_all', NULL, TRUE );
				break;
		}
		$this->data['breadcumb'] = $this->load->view($this->template.'breadcumb', $breadcumbs, TRUE);
		$this->content = $this->load->view($this->template.'dashboard', $this->data, TRUE);
	}
	
	private function _admingroups()
	{
		$action = $this->input->post('action');
		
		$breadcumbs['items'] = array( 'admingroups' => 'Roles' );
		
		if( !isset( $this->mAdmingroups ) ) $this->load->model('Admingroups_model', 'mAdmingroups');
		
		switch ( $action ) {
			case 'add':
			case 'edit':
				
				$this->data['page_subtitle'] = 'Add New Role';
				$breadcumbs['items']['#'] = 'Add New Role';
				
				$admingroup = NULL;
				$admingroup['my_roles'] = NULL;
				if( $action == 'edit' ){
					$this->data['page_subtitle'] = 'Edit Role';
					$breadcumbs['items']['#'] = 'Edit Role';
					$admingroup['admingroup'] = $this->mAdmingroups->admingroup_detail( $this->input->post('id') );
					
					if( !isset( $this->mAGR ) ) $this->load->model('Admingrouproles_model', 'mAGR');
					$admingroup['my_roles'] = $this->mAGR->get_roles_by_group_id( $this->input->post('id') );
				}
				
				// get all the roles
				if( !isset( $this->mRoles ) ) $this->load->model('Roles_model', 'mRoles');
				$admingroup['roles'] = $this->mRoles->get_all();
				
				// get the category details
				$this->data['content'] = $this->load->view( $this->template.'admingroups/add', $admingroup, TRUE );
				$this->js = $this->load->view( $this->template.'admingroups/js/add', $admingroup, TRUE );	
				
				break;
				
			case 'read':
				break;
			// by default, it is to view all
			case 'delete':
				$this->mAdmingroups->delete_admingroup();
			case 'view':
			case 'save_new':
				// add to database here
				$this->mAdmingroups->save_new();
			default:
				
				$this->data['page_subtitle'] = 'View Role';
				$breadcumbs['items']['#'] = 'View All';
				
				// get the list of all the categories
				
				$params['admingroups'] = $this->mAdmingroups->get_all();
				
				$this->data['content'] = $this->load->view( $this->template.'admingroups/view_all', $params, TRUE );
				$this->js = $this->load->view( $this->template.'admingroups/js/view_all', NULL, TRUE );
				$this->css = $this->load->view( $this->template.'admingroups/css/view_all', NULL, TRUE );
				break;
		}
		$this->data['breadcumb'] = $this->load->view($this->template.'breadcumb', $breadcumbs, TRUE);
		$this->content = $this->load->view($this->template.'dashboard', $this->data, TRUE);
	}
	
	private function _categories()
	{
		// get my roles
		
		$action = $this->input->post('action');
		
		$breadcumbs['items'] = array( 'categories' => 'Job Titles' );
		if( !isset( $this->mCategories ) ) $this->load->model('Categories_model', 'mCategories');
		
		switch ( $action ) {
			case 'add':
			case 'edit':
				
				$this->data['page_subtitle'] = 'Add New Job Title';
				$breadcumbs['items']['#'] = 'Add New Job Title';
				
				$category = NULL;
				if( $action == 'edit' ){
					$this->data['page_subtitle'] = 'Edit Job Title';
					$breadcumbs['items']['#'] = 'Edit Job Title';
					$category['category'] = $this->mCategories->category_detail( $this->input->post('id') );
				}
				
				// get the category details
				if( (!in_array( 'Add Category' , $this->my_roles) && $action == 'add') || (!in_array( 'Edit Category' , $this->my_roles) && $action == 'edit')){
					$this->data['content'] = 'Restricted Access.';
				} else {
					$this->data['content'] = $this->load->view( $this->template.'categories/add', $category, TRUE );
				}
				
				$this->js = $this->load->view( $this->template.'categories/js/add', $category, TRUE );	
				
				break;
				
			case 'read':
				break;
			// by default, it is to view all
			case 'delete':
				$this->mCategories->delete_category();
			case 'view':
			case 'save_new':
				// add to database here
				$this->mCategories->save_new();
			default:
				
				$this->data['page_subtitle'] = 'View Job Titles';
				$breadcumbs['items']['#'] = 'View All';
				
				// get the list of all the categories
				
				$params['categories'] = $this->mCategories->get_all();
				$params['my_roles'] = $this->my_roles;
				
				$this->data['content'] = $this->load->view( $this->template.'categories/view_all', $params, TRUE );
				$this->js = $this->load->view( $this->template.'categories/js/view_all', NULL, TRUE );
				$this->css = $this->load->view( $this->template.'categories/css/view_all', NULL, TRUE );
				break;
		}
		$this->data['breadcumb'] = $this->load->view($this->template.'breadcumb', $breadcumbs, TRUE);
		$this->content = $this->load->view($this->template.'dashboard', $this->data, TRUE);
	}

	private function _roles()
	{
		$action = $this->input->post('action');
		
		$breadcumbs['items'] = array( 'roles' => 'Role Settings' );
		
		if( !isset( $this->mRoles ) ) $this->load->model('Roles_model', 'mRoles');
		
		switch ( $action ) {
			case 'add':
			case 'edit':
				
				$this->data['page_subtitle'] = 'Add New Role Setting';
				$breadcumbs['items']['#'] = 'Add New Role Setting';
				
				$role = NULL;
				if( $action == 'edit' ){
					$this->data['page_subtitle'] = 'Edit Role Setting';
					$breadcumbs['items']['#'] = 'Edit Role Setting';
					$role['role'] = $this->mRoles->role_detail( $this->input->post('id') );
				}
				
				// get the category details
				$this->data['content'] = $this->load->view( $this->template.'roles/add', $role, TRUE );
				$this->js = $this->load->view( $this->template.'roles/js/add', $role, TRUE );	
				
				break;
				
			case 'read':
				break;
			// by default, it is to view all
			case 'delete':
				$this->mRoles->delete_role();
			case 'view':
			case 'save_new':
				// add to database here
				$this->mRoles->save_new();
			default:
				
				$this->data['page_subtitle'] = 'View Role Settings';
				$breadcumbs['items']['#'] = 'View All';
				
				// get the list of all the categories
				
				$params['roles'] = $this->mRoles->get_all();
				
				$this->data['content'] = $this->load->view( $this->template.'roles/view_all', $params, TRUE );
				$this->js = $this->load->view( $this->template.'roles/js/view_all', NULL, TRUE );
				$this->css = $this->load->view( $this->template.'roles/css/view_all', NULL, TRUE );
				break;
		}
		$this->data['breadcumb'] = $this->load->view($this->template.'breadcumb', $breadcumbs, TRUE);
		$this->content = $this->load->view($this->template.'dashboard', $this->data, TRUE);
	}
	
	private function _exams()
	{
		$action = $this->input->post('action');
		
		$breadcumbs['items'] = array( 'exams' => 'Exams' );
		
		if( !isset( $this->mExams ) ) $this->load->model('Modules_model', 'mExams');
		
		switch ( $action ) {
			case 'add':
			case 'edit':
				
				$this->data['page_subtitle'] = 'Add New Exam';
				$breadcumbs['items']['#'] = 'Add New Exam';
				
				$exam = NULL;
				if( $action == 'edit' ){
					$this->data['page_subtitle'] = 'Edit Exam';
					$breadcumbs['items']['#'] = 'Edit Exam';
					$exam['exam'] = $this->mExams->exam_detail( $this->input->post('id') );
					
					// get the questions 
					if( !isset( $this->mQuestions ) ) $this->load->model('Questions_model', 'mQuestions');
					$exam['questions'] = $this->mQuestions->get_questions( $this->input->post('id') );
					
					// get the options
					if( !isset( $this->mChoices ) ) $this->load->model('Choices_model', 'mChoices');
					$exam['choices'] = $this->mChoices->get_choices_by_exam( $this->input->post('id') );
				}
				
				// get the category details
				if( (!in_array( 'Add Exam' , $this->my_roles) && $action == 'add') || (!in_array( 'Edit Exam' , $this->my_roles) && $action == 'edit')){
					$this->data['content'] = 'Restricted Access.';
				} else {
					$this->data['content'] = $this->load->view( $this->template.'exams/add', $exam, TRUE );
				}
				
				$this->data['content'] = $this->load->view( $this->template.'exams/add', $exam, TRUE );
				$this->js = $this->load->view( $this->template.'exams/js/add', $exam, TRUE );
				$this->css = $this->load->view( $this->template.'exams/css/add', $exam, TRUE );	
				
				break;
				
			case 'read':
				break;
			// by default, it is to view all
			case 'delete':
				$this->mExams->delete_exam();
			case 'view':
			case 'save_new':
				// add to database here
				$this->mExams->save_new();
			default:
				
				$this->data['page_subtitle'] = 'View Exams';
				$breadcumbs['items']['#'] = 'View All';
				
				// get the list of all the module categories
				
				$params['exams'] = $this->mExams->get_all();
				$params['my_roles'] = $this->my_roles;
				
				$this->data['content'] = $this->load->view( $this->template.'exams/view_all', $params, TRUE );
				$this->js = $this->load->view( $this->template.'exams/js/view_all', NULL, TRUE );
				$this->css = $this->load->view( $this->template.'exams/css/view_all', NULL, TRUE );
				break;
		}
		$this->data['breadcumb'] = $this->load->view($this->template.'breadcumb', $breadcumbs, TRUE);
		$this->content = $this->load->view($this->template.'dashboard', $this->data, TRUE);
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect('admin');
	}
	
	private function _dashboard()
	{
		$data = $this->data;
		$this->content = $this->load->view($this->template.'dashboard', $data, TRUE);
	}
	
	private function _finalize()
	{
		$data['title'] = $this->title;
		$data['content'] = $this->content;
		$data['js'] = $this->js;
		$data['css'] = $this->css;
		$this->load->view($this->template.'template', $data);
	}
	
	private function _login()
	{
		$this->title = 'Sign in';
		$this->content = $this->load->view($this->template.'login', NULL, TRUE);
		$this->js = $this->load->view($this->template.'js/login', NULL, TRUE);
	}
	
	private function _check_logged()
	{
		return $this->session->userdata('logged_in');
	}
}