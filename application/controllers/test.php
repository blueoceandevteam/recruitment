<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends CI_Controller {
	
	public function index(){
		$sql = "SELECT * FROM lists";
		$query = $this->db->query( $sql );
		$data['lists'] = $query->num_rows() > 0 ?  $query->result() : NULL;
		
		$sql = "SELECT * FROM mylist";
		$query = $this->db->query( $sql );
		$data['mylist'] = $query->num_rows() > 0 ?  $query->result() : NULL;
		
		$this->load->view('test', $data);
	}
	
	public function add()
	{
		$result = 'false';
		$id = $_POST['id'];
		if( !empty( $id ) ){
			$this->db->insert('mylist', array('list_id' => $id));
			$result = 'true';
		}
		return $result;
	}
	
	
}