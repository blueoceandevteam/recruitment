-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 23, 2015 at 08:51 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `recruitment`
--

-- --------------------------------------------------------

--
-- Table structure for table `admingroups`
--

CREATE TABLE IF NOT EXISTS `admingroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `added_by` int(11) NOT NULL DEFAULT '0',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `admingroups`
--

INSERT INTO `admingroups` (`id`, `name`, `description`, `deleted`, `added_by`, `modified_by`, `date_added`, `date_modified`) VALUES
(1, 'Super Admins', 'Super Administrators', 0, 1, 0, '2015-01-22 00:55:53', '2015-01-23 00:20:57'),
(3, 'Editors', '', 0, 2, 0, '2015-01-22 01:10:53', '2015-01-22 03:29:23'),
(4, 'Viewer', '', 1, 1, 1, '2015-01-22 01:12:38', '2015-01-22 00:18:13');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `added_by` int(11) NOT NULL DEFAULT '0',
  `modified_by` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `first_name`, `last_name`, `password`, `deleted`, `added_by`, `modified_by`, `date_added`, `date_modified`) VALUES
(1, 'allan', 'Allan', 'Ninal', 'ywZl5dBS2R7In4u4GkT4MhLaJxfbGn/9ev9Yw82nFAA3ad9UaTe3uTS+S2QtdLTUmTo6AZ2zVMVKSJNZiZLuXA==', 0, 1, 2, '2015-01-14 12:37:35', '2015-01-22 01:59:16'),
(2, 'admin', 'Blue Ocean', 'BPO', 'toTeiU8MPOwXQ7R+tb4D+7ZUcS6DAqPwYgExXXGtwU4Fwy3hYJ7MDsSTaj9usDikfeMWEbXM73cCgPCDybW/WQ==', 0, 1, 2, '2015-01-22 02:54:35', '2015-01-22 05:42:32');

-- --------------------------------------------------------

--
-- Table structure for table `admins_groups`
--

CREATE TABLE IF NOT EXISTS `admins_groups` (
  `admin_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`admin_id`),
  UNIQUE KEY `admin_id` (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins_groups`
--

INSERT INTO `admins_groups` (`admin_id`, `group_id`, `added_by`, `modified_by`, `date_added`, `date_modified`) VALUES
(1, 1, 0, 2, '0000-00-00 00:00:00', '2015-01-22 02:07:34'),
(2, 3, 0, 2, '0000-00-00 00:00:00', '2015-01-23 02:56:50');

-- --------------------------------------------------------

--
-- Table structure for table `admin_group_roles`
--

CREATE TABLE IF NOT EXISTS `admin_group_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_group_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `admin_group_roles`
--

INSERT INTO `admin_group_roles` (`id`, `admin_group_id`, `role_id`, `date_added`) VALUES
(7, 1, 4, '2015-01-23 00:20:57'),
(8, 1, 1, '2015-01-23 00:20:57'),
(9, 1, 7, '2015-01-23 00:20:57'),
(10, 1, 6, '2015-01-23 00:20:57'),
(11, 1, 2, '2015-01-23 00:20:57'),
(12, 1, 9, '2015-01-23 00:20:57'),
(13, 1, 5, '2015-01-23 00:20:57'),
(14, 1, 3, '2015-01-23 00:20:57'),
(15, 1, 8, '2015-01-23 00:20:57');

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE IF NOT EXISTS `answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `applicants`
--

CREATE TABLE IF NOT EXISTS `applicants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `passcode` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `status` enum('pending','partial','done') NOT NULL DEFAULT 'pending',
  `deleted` tinyint(1) DEFAULT '0',
  `added_by` tinyint(4) NOT NULL DEFAULT '0',
  `modified_by` tinyint(4) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `applicants`
--

INSERT INTO `applicants` (`id`, `cat_id`, `code`, `passcode`, `first_name`, `last_name`, `status`, `deleted`, `added_by`, `modified_by`, `date_added`, `date_modified`) VALUES
(1, '1', 'WD00001', 'WnvhYFQ2vO', 'Allan', 'Ninal', 'pending', 0, 1, 1, '2015-01-19 03:36:09', '2015-01-19 07:36:49');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `added_by` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `code`, `added_by`, `date_added`, `date_modified`) VALUES
(1, 'Web Developer', 'WD', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Content Writer', 'CW', '1', '2015-01-15 03:44:45', '2015-01-14 18:44:45'),
(4, 'Human Resource', 'HR', '1', '2015-01-15 03:51:54', '2015-01-14 18:51:54'),
(5, 'Sotware Engineer', 'SE', '1', '2015-01-15 03:55:05', '2015-01-14 18:55:05'),
(7, 'Researcher', 'RS', '1', '2015-01-15 04:03:08', '2015-01-14 19:03:08'),
(8, 'Creative Marketing', 'CM', '1', '2015-01-15 04:05:02', '2015-01-14 19:05:02'),
(9, 'Content Analyst', 'CA', '1', '2015-01-15 04:58:57', '2015-01-14 19:58:57'),
(10, 'Application Tester', 'AT', '1', '2015-01-15 05:07:07', '2015-01-14 20:11:05');

-- --------------------------------------------------------

--
-- Table structure for table `choices`
--

CREATE TABLE IF NOT EXISTS `choices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `is_correct` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('5695543310632ffe1a0d960f8cce548d', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.99 Safari/537.36', 1421998004, 'a:17:{s:9:"user_data";s:0:"";s:9:"logged_in";i:1;s:8:"username";s:5:"allan";s:7:"user_id";s:1:"1";s:12:"new_category";N;s:13:"edit_category";N;s:6:"delete";N;s:13:"new_applicant";N;s:14:"edit_applicant";N;s:10:"new_module";N;s:11:"edit_module";N;s:14:"new_admingroup";N;s:15:"edit_admingroup";N;s:9:"new_admin";N;s:10:"edit_admin";N;s:8:"new_role";N;s:9:"edit_role";N;}'),
('b6ede9f7c022cc335a6ccd2a7ce90c78', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0', 1421997085, 'a:9:{s:9:"user_data";s:0:"";s:9:"logged_in";i:1;s:8:"username";s:5:"allan";s:7:"user_id";s:1:"1";s:12:"new_category";N;s:13:"edit_category";N;s:6:"delete";N;s:13:"new_applicant";N;s:14:"edit_applicant";N;}');

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

CREATE TABLE IF NOT EXISTS `exams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `passing_grade` decimal(10,0) NOT NULL,
  `added_by` varchar(255) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exams`
--

INSERT INTO `exams` (`id`, `name`, `description`, `published`, `passing_grade`, `added_by`, `modified_by`, `date_added`, `date_modified`) VALUES
(1, 'Module 1', 'MD2', 1, '0', '1', 0, '2015-01-21 06:45:58', '2015-01-21 05:45:58');

-- --------------------------------------------------------

--
-- Table structure for table `exam_categories`
--

CREATE TABLE IF NOT EXISTS `exam_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `added_by` tinyint(4) NOT NULL,
  `modified_by` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `position` tinyint(4) NOT NULL,
  `selected` tinyint(1) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `added_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `code`, `description`, `added_by`, `modified_by`, `date_added`, `date_modified`) VALUES
(1, 'Add Category', 'Add admin categories', 2, 0, '2015-01-22 05:38:45', '2015-01-22 04:38:45'),
(2, 'Delete Category', 'Delete admin categories', 2, 0, '2015-01-22 05:39:15', '2015-01-22 04:47:59'),
(3, 'Edit Category', 'Edit admin categories', 2, 0, '2015-01-22 05:39:39', '2015-01-22 04:39:39'),
(4, 'Add Applicant', 'User can add applicants', 1, 0, '2015-01-23 01:06:01', '2015-01-23 00:06:01'),
(5, 'Edit Applicant', 'User can edit applicant', 1, 0, '2015-01-23 01:09:12', '2015-01-23 00:09:12'),
(6, 'Delete Applicant', 'User can remove applicant', 1, 0, '2015-01-23 01:09:42', '2015-01-23 00:09:42'),
(7, 'Add User', 'User can add another user', 1, 0, '2015-01-23 01:19:11', '2015-01-23 00:19:11'),
(8, 'Edit User', 'User can modify another user', 1, 0, '2015-01-23 01:19:27', '2015-01-23 00:19:27'),
(9, 'Delete User', 'User can remove another user', 1, 0, '2015-01-23 01:19:54', '2015-01-23 00:19:54');

-- --------------------------------------------------------

--
-- Table structure for table `user_exams`
--

CREATE TABLE IF NOT EXISTS `user_exams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `quz_id` int(11) NOT NULL,
  `num_correct` tinyint(4) NOT NULL,
  `total_questions` tinyint(4) NOT NULL,
  `status` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
